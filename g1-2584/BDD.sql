-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: mysql-g1-2584.alwaysdata.net
-- Generation Time: Apr 02, 2018 at 08:17 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `g1-2584_jeux_2584`
--

-- --------------------------------------------------------

--
-- Table structure for table `Partie`
--

CREATE TABLE `Partie` (
  `id` int(10) UNSIGNED NOT NULL,
  `score_joueur_un` int(11) NOT NULL,
  `score_joueur_deux` int(11) NOT NULL,
  `nombre_coups_joueur_un` int(11) NOT NULL,
  `nombre_coups_joueur_deux` int(11) NOT NULL,
  `tuile_maximum_joueur_un` int(11) NOT NULL,
  `tuile_maximum_joueur_deux` int(11) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `Partie`
--

INSERT INTO `Partie` (`id`, `score_joueur_un`, `score_joueur_deux`, `nombre_coups_joueur_un`, `nombre_coups_joueur_deux`, `tuile_maximum_joueur_un`, `tuile_maximum_joueur_deux`, `date`) VALUES
(17, 436, 322, 82, 82, 7, 6, '2018-03-19 19:33:19'),
(18, 556, 610, 117, 117, 7, 8, '2018-03-19 19:33:30'),
(19, 468, 524, 100, 100, 7, 8, '2018-03-19 19:33:35'),
(20, 238, 236, 65, 65, 5, 6, '2018-03-19 19:33:41'),
(21, 419, 483, 102, 102, 7, 7, '2018-03-19 19:33:48'),
(22, 388, 423, 90, 90, 7, 7, '2018-03-19 19:33:54'),
(23, 358, 381, 84, 84, 7, 7, '2018-03-19 19:34:03'),
(24, 383, 430, 93, 93, 6, 7, '2018-03-19 19:34:09'),
(25, 617, 563, 115, 115, 8, 8, '2018-03-19 19:34:16'),
(26, 370, 309, 80, 80, 7, 6, '2018-03-19 19:34:22'),
(27, 303, 353, 78, 78, 6, 7, '2018-03-19 19:34:27'),
(28, 243, 294, 69, 69, 6, 7, '2018-03-19 19:34:33'),
(29, 615, 574, 119, 119, 8, 7, '2018-03-19 19:34:39'),
(30, 340, 340, 85, 85, 7, 6, '2018-03-19 19:34:46'),
(31, 224, 200, 63, 63, 6, 6, '2018-03-19 19:34:52'),
(32, 234, 308, 64, 64, 6, 7, '2018-03-19 19:34:57'),
(33, 293, 256, 74, 74, 6, 5, '2018-03-19 19:35:04'),
(34, 328, 362, 81, 81, 6, 7, '2018-03-19 19:35:11'),
(35, 535, 502, 104, 104, 8, 8, '2018-03-19 19:35:17'),
(36, 283, 261, 69, 69, 7, 6, '2018-03-19 19:35:23'),
(37, 214, 182, 58, 58, 6, 5, '2018-03-19 19:35:31'),
(38, 214, 251, 63, 63, 6, 6, '2018-03-19 19:35:37'),
(39, 331, 355, 78, 78, 7, 7, '2018-03-19 19:36:06'),
(40, 615, 641, 121, 121, 8, 8, '2018-03-19 19:36:12'),
(41, 407, 522, 94, 94, 7, 8, '2018-03-19 19:36:19'),
(42, 273, 322, 76, 76, 6, 7, '2018-03-19 19:36:25'),
(43, 379, 435, 90, 90, 6, 7, '2018-03-19 19:36:31'),
(44, 325, 289, 76, 76, 7, 6, '2018-03-19 19:36:37'),
(45, 311, 274, 73, 73, 7, 6, '2018-03-19 19:36:42'),
(46, 705, 731, 132, 132, 7, 8, '2018-03-19 19:36:50'),
(47, 537, 512, 109, 109, 7, 7, '2018-03-19 19:36:55'),
(48, 249, 301, 67, 67, 6, 7, '2018-03-19 19:37:01'),
(49, 484, 446, 96, 96, 8, 7, '2018-03-19 19:37:10'),
(50, 562, 547, 114, 114, 7, 7, '2018-03-19 19:37:56'),
(51, 669, 667, 131, 131, 8, 8, '2018-03-19 19:38:02'),
(52, 396, 404, 93, 93, 7, 7, '2018-03-19 19:38:09'),
(53, 372, 423, 83, 83, 7, 7, '2018-03-20 13:50:32'),
(54, 939, 791, 142, 142, 9, 8, '2018-03-20 13:51:04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Partie`
--
ALTER TABLE `Partie`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Partie`
--
ALTER TABLE `Partie`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
