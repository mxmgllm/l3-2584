/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controleur;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.stage.Stage;
import vue.VueMenuPrincipal;
import vue.VueMenuTypeJoueur;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import modele.Partie;

/**
 * Racine du programme. Controleur gérant les interactions entre le contrôleur
 * de la partie et le contrôleur de la BDD, et le menu principal ainsi que le
 * menu de choix du type de joueur pour les nouvelles parties.
 *
 * @author emarquer
 */
public class ControleurMenuPrincipal extends Controleur {

    private ControleurPartie controleurPartie;
    private ControleurBDD controleurBDD;
    private VueMenuPrincipal vueMenuPrincipal;
    private VueMenuTypeJoueur vueTypeJoueur;

    private EventHandler<ActionEvent> gestionnaireNouvellePartie;
    private EventHandler<ActionEvent> gestionnaireLancerPartie;
    private EventHandler<ActionEvent> gestionnaireChargerPartie;
    private EventHandler<ActionEvent> gestionnairePartiesPrecedentes;
    private EventHandler<ActionEvent> gestionnaireRetourScene;
    private EventHandler<ActionEvent> gestionnaireQuitter;

    /**
     * Créé le controleur du menu principal
     *
     * @param theatre théâtre dans lequel créer les scènes du jeu
     */
    public ControleurMenuPrincipal(Stage theatre) {
        super(theatre);

        this.vueMenuPrincipal = new VueMenuPrincipal();
        this.vueTypeJoueur = new VueMenuTypeJoueur();
        this.controleurBDD = new ControleurBDD(this);

        initialiserGestionnaires();
    }

    /**
     * Méthode appelée par l'action du bouton de lancement de partie, change la
     * scène a celle du contrôleur de partie
     */
    private void lancerPartie() {
        this.controleurPartie = new ControleurPartie(
                this.vueTypeJoueur.retournerTypeJoueurUn(),
                this.vueTypeJoueur.retournerTypeJoueurDeux(),
                this.gestionnaireNouvellePartie,
                this.gestionnaireChargerPartie,
                this.gestionnaireQuitter,
                this);

        // Initialisation et affichage des éléments d'interface
        this.controleurPartie.demarerGUI();
    }

    /**
     * Crée les gestionnaires d'évènements pour : le bouton de lancement de
     * partie; le bouton de nouvelle partie (renvoie vers le menu de choix du
     * type de joueur; le bouton de chargement de partie; le bouton d'affichage
     * des anciennes parties; le bouton quitter.
     */
    private void initialiserGestionnaires() {
        this.gestionnaireNouvellePartie
                = (ActionEvent evenement) -> {
                    nouvellePartie();
                };
        this.gestionnaireLancerPartie
                = (ActionEvent evenement) -> {
                    lancerPartie();
                };
        this.gestionnaireChargerPartie
                = (ActionEvent evenement) -> {
                    chargerPartie();
                };
        this.gestionnairePartiesPrecedentes
                = (ActionEvent evenement) -> {
                    partiesPrecedentes(this.getSceneActuelle());
                };
        this.gestionnaireRetourScene
                = (ActionEvent evenement) -> {
                    retourScenePrecedente();
                };
        this.gestionnaireQuitter
                = (ActionEvent evenement) -> {
                    quitter();
                };
    }

    /**
     * Création d'un contrôleur pour une partie chargée, change la scène a celle
     * du contrôleur de partie
     *
     * @param partie partie chargée
     */
    private void demarrerViaChargement(Partie partie) {
        // Création du nouveau contrôleur
        this.controleurPartie = new ControleurPartie(partie,
                gestionnaireNouvellePartie,
                gestionnaireChargerPartie,
                gestionnaireQuitter,
                this);

        // Initialisation et affichage des éléments d'interface
        this.controleurPartie.demarerGUI();
    }

    /**
     * Initialise la GUI et affiche le théâtre
     *
     * @param theatre Théâtre a laquelle ajouter la scène
     */
    public void demarerGUI(Stage theatre) {
        // creation de la scene et ajout au theatre
        Scene scene = vueMenuPrincipal.initialiserScene(
                this.gestionnaireNouvellePartie,
                this.gestionnaireChargerPartie,
                this.gestionnairePartiesPrecedentes,
                this.gestionnaireQuitter);

        // changement de scene
        this.changerScene(theatre, scene);
    }

    /**
     * Méthode appelée par l'action du bouton quitter, quitte l'application
     */
    private void quitter() {
        System.exit(0);
    }

    /**
     * Méthode appelée par l'action du bouton de chargement de partie, charge la
     * partie
     */
    private void chargerPartie() {
        ObjectInputStream fluxLectureObjets = null;
        Partie partie;

        // Ouverture et lecture de flux
        try {
            // Ouverture du flux de fichier
            final FileInputStream fluxLectureFichier = new FileInputStream("partie.ser");
            fluxLectureObjets = new ObjectInputStream(fluxLectureFichier);

            // Lecture de la partie
            partie = (Partie) fluxLectureObjets.readObject();

            // Démarage de la partie
            this.demarrerViaChargement(partie);

            // Erreur de flux (fichier non existant)
        } catch (final java.io.IOException exception) {
            exception.printStackTrace();

            // Erreur de lecture de la partie
        } catch (final ClassNotFoundException exception) {
            exception.printStackTrace();

            // Flermeture du flux à la fin de la lecture
        } finally {
            try {
                if (fluxLectureObjets != null) {
                    fluxLectureObjets.close();
                }
            } catch (final IOException exception) {
                exception.printStackTrace();
            }
        }
    }

    /**
     * Méthode appelée par l'action du bouton de nouvelle partie, change la
     * scène a celle du menu de nouvelle partie
     */
    private void nouvellePartie() {
        // Création de la scène et changement de scène du théâtre
        Scene scene = this.vueTypeJoueur.initialiserScene(
                this.gestionnaireLancerPartie,
                this.gestionnaireRetourScene);

        // Changement de scène
        this.changerScene(scene);
    }

    /**
     * Méthode appelée par l'action du bouton d'affichage des parties
     * précédentes, change la scène a celle de la liste des parties précédentes
     *
     * @param sceneActuelle scène actuellement affichée, sera réaffichée en
     * sortant de la vue des parties précédentes
     */
    public void partiesPrecedentes(Scene sceneActuelle) {
        // Affichage de la vue des parties précédentes
        this.controleurBDD.afficherScore(sceneActuelle);
    }
}
