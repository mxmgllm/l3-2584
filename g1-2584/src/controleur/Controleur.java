/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controleur;

import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Contrôleur de base, avec gestion du changement de scène.
 *
 * @author emarquer
 */
abstract class Controleur {

    private Scene scenePrecedente;
    private Scene sceneActuelle;
    private Stage theatre;

    /**
     * Crée le contrôleur
     *
     * @param theatre théâtre dans lequel afficher les scènes
     */
    public Controleur(Stage theatre) {
        this.scenePrecedente = null;
        this.sceneActuelle = null;
        this.theatre = theatre;
    }

    /**
     * Crée le contrôleur
     *
     * @param controleur contrôleur précédent
     */
    public Controleur(Controleur controleur) {
        this(controleur != null ? controleur.theatre : null);
    }

    /**
     * Méthode appelée par le bouton de retour à la scène précédent, change la
     * scène active du théâtre à la scène enregistrée
     *
     * @return {@code true} si la scène précédente à pu être restaurée,
     * {@code false} sinon.
     */
    protected boolean retourScenePrecedente() {
        if (this.scenePrecedente != null && this.theatre != null) {
            // Définit la vue précédente comme scène actuelle du théatre
            this.changerScene(this.scenePrecedente);

            return true;
        } else {
            return false;
        }
    }

    /**
     * Change la scène du théâtre
     *
     * @param theatre théâtre dont il faut changer la scène
     * @param scenePrecedente précédente scène du théâtre
     * @param sceneNouvelle nouvelle scène du théâtre
     */
    protected final void changerScene(
            Stage theatre,
            Scene scenePrecedente,
            Scene sceneNouvelle) {
        // Mémorise le théatre
        this.theatre = theatre;

        this.changerScene(scenePrecedente, sceneNouvelle);
    }

    /**
     * Change la scène du théâtre
     *
     * @param theatre théâtre dont il faut changer la scène
     * @param sceneNouvelle nouvelle scène du théâtre
     */
    protected final void changerScene(
            Stage theatre,
            Scene sceneNouvelle) {
        // Mémorise le théatre
        this.theatre = theatre;

        changerScene(sceneNouvelle);
    }

    /**
     * Change la scène du théâtre
     *
     * @param scenePrecedente précédente scène du théâtre
     * @param sceneNouvelle nouvelle scène du théâtre
     */
    protected final void changerScene(
            Scene scenePrecedente,
            Scene sceneNouvelle) {
        // Mémorise la scene précédente et la nouvelle scene
        this.scenePrecedente = scenePrecedente;
        this.sceneActuelle = sceneNouvelle;

        // Définit la vue de la base de données comme scène actuelle du théatre
        this.theatre.setScene(sceneNouvelle);
        this.theatre.sizeToScene();
    }

    /**
     * Change la scène du théâtre
     *
     * @param sceneNouvelle nouvelle scène du théâtre
     */
    protected final void changerScene(
            Scene sceneNouvelle) {
        changerScene(this.sceneActuelle, sceneNouvelle);
    }

    /**
     * @return la scenePrecedente
     */
    public Scene getScenePrecedente() {
        return scenePrecedente;
    }

    /**
     * @return la sceneActuelle
     */
    public Scene getSceneActuelle() {
        return sceneActuelle;
    }

    /**
     * @return le theatre
     */
    public Stage getTheatre() {
        return theatre;
    }
}
