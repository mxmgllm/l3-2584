/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controleur;

import java.util.ArrayList;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import modele.Partie;
import modele.bdd.PartieBDD;
import vue.VueBDD;

/**
 * Controleur gérant les interactions avec la BDD et la vue de la BDD
 *
 * @author emarquer
 */
public class ControleurBDD extends Controleur {

    private VueBDD vueBDD;

    public ControleurBDD(Controleur controleur) {
        super(controleur);
        this.vueBDD = new VueBDD();
    }

    /**
     * Affiche la vue des 10 dernières parties de la BDD
     *
     * @param scenePrecedente
     */
    public void afficherScore(Scene scenePrecedente) {
        // Creation de la scene
        Scene sceneBDD = vueBDD.initialiserScene(
                recupererPartiesBDD(),
                (ActionEvent actionBouton) -> {
                    retourScenePrecedente();
                });

        // Changement de scene
        this.changerScene(scenePrecedente, sceneBDD);
    }

    /**
     * Ajoute une partie à la BDD
     *
     * @param partie partie à ajouter à la BDD
     */
    public static void enregistrerPartieBDD(Partie partie) {
        PartieBDD partieBDD = new PartieBDD(partie);
        PartieBDD.insererPartie(partieBDD);
    }

    /**
     * Ajoute une partie à la BDD
     *
     * @param partie partie à ajouter à la BDD
     */
    private static ArrayList<PartieBDD> recupererPartiesBDD() {
        return PartieBDD.selectionnerParties();
    }
}
