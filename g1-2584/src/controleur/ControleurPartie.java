package controleur;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import modele.Construction;
import modele.Jeu;
import modele.Parametres;
import modele.Partie;
import vue.jeu.VuePartie;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * Controleur gérant les interactions avec le modèle de la partie et la vue de
 * la partie
 *
 * @author mg
 */
public class ControleurPartie extends Controleur implements Parametres {

    private final VuePartie vuePartie;
    private Partie partie;
    private final String typeJoueurUn;
    private final String typeJoueurDeux;

    private EventHandler<ActionEvent> gestionnaireAnnulationUn;
    private EventHandler<ActionEvent> gestionnaireAnnulationDeux;
    private EventHandler<ActionEvent> gestionnaireSauvegarde;
    private EventHandler<KeyEvent> gestionnaireClavier;
    private boolean interdireActionJoueur;

    /**
     * Crée le contrôleur de partie en initialisant la partie et en créant les
     * vues
     *
     * @param typeJoueurUn type du joueur un
     * @param typeJoueurDeux type du joueur deux
     * @param gestionnaireNouvellePartie gestionnaire d'évènements pour le
     * lancement de partie
     * @param gestionnaireChargerPartie gestionnaire d'évènements pour le
     * chargement de partie
     * @param gestionnaireQuitter gestionnaire d'évènements pour quitter le jeu
     * @param controleur contrôleur précédent
     */
    public ControleurPartie(String typeJoueurUn, String typeJoueurDeux,
            EventHandler<ActionEvent> gestionnaireNouvellePartie,
            EventHandler<ActionEvent> gestionnaireChargerPartie,
            EventHandler<ActionEvent> gestionnaireQuitter,
            ControleurMenuPrincipal controleur) {
        this(Construction.creerUnePartie(typeJoueurUn, typeJoueurDeux),
                gestionnaireNouvellePartie,
                gestionnaireChargerPartie,
                gestionnaireQuitter,
                controleur);
    }

    /**
     * Crée le contrôleur de partie à partir d'une partie et en créant les vues
     *
     * @param partie partie à charger
     * @param gestionNouvellePartie gestionnaire d'évènements pour le lancement
     * de partie
     * @param gestionChargerPartie gestionnaire d'évènements pour le chargement
     * de partie
     * @param gestionQuitter gestionnaire d'évènements pour quitter le jeu
     * @param controleur contrôleur précédent
     */
    public ControleurPartie(Partie partie,
            EventHandler<ActionEvent> gestionNouvellePartie,
            EventHandler<ActionEvent> gestionChargerPartie,
            EventHandler<ActionEvent> gestionQuitter,
            ControleurMenuPrincipal controleur) {
        super(controleur);
        this.partie = partie;

        // extraction du type de joueur
        this.typeJoueurUn = this.partie.getJeu().retournerNomClasseJoueurUn();
        this.typeJoueurDeux
                = this.partie.getJeu().retournerNomClasseJoueurDeux();

        // Création des gestionnaires d'évenements
        initialiserGestionnaires();

        // Création de la vue
        EventHandler<ActionEvent> gestionPartiesPrecedentes
                = (ActionEvent evenement) -> {
                    controleur.partiesPrecedentes(this.getSceneActuelle());
                };
        this.vuePartie = new VuePartie(
                typeJoueurUn,
                typeJoueurDeux,
                gestionnaireAnnulationUn,
                gestionnaireAnnulationDeux,
                gestionNouvellePartie,
                gestionChargerPartie,
                gestionnaireSauvegarde,
                gestionPartiesPrecedentes,
                gestionQuitter);

        this.miseAJourVue();

    }

    /**
     * @param partie la nouvelle partie
     */
    public void setPartie(Partie partie) {
        this.partie = partie;
    }

    /**
     * Crée les gestionnaires d'évènements pour : le bouton de lancement de
     * partie; le bouton de nouvelle partie (renvoie vers le menu de choix du
     * type de joueur; le bouton de chargement de partie; le bouton d'affichage
     * des anciennes parties; le bouton quitter.
     */
    private void initialiserGestionnaires() {
        this.gestionnaireAnnulationUn
                = (ActionEvent evenement) -> {
                    annulerJoueurUn();
                };
        this.gestionnaireAnnulationDeux
                = (ActionEvent evenement) -> {
                    annulerJoueurDeux();
                };
        this.gestionnaireSauvegarde
                = (ActionEvent evenement) -> {
                    sauvegarder();
                };
        this.gestionnaireClavier
                = (final KeyEvent evenement) -> {
                    appliquerMouvement(
                            evenement.getText(),
                            evenement.getCode());
                };
    }

    /**
     * Mise à jour de la vue
     */
    private void miseAJourVue() {
        this.vuePartie.mettreAJourVueJeu(
                partie.getJeu().retournerScoreUn(),
                partie.getJeu().retournerScoreDeux(),
                partie.getJeu().retournerNbCoupUn(),
                partie.getJeu().retournerNbCoupDeux(),
                partie.getJeu().retournerGrilleValeurUne(),
                partie.getJeu().retournerGrilleValeurDeux());

        // Mise à jours des déplacements dans la vue
        this.vuePartie.mettreAJourDeplacements(
                partie.getJeu().retournerDeplacementsUn(),
                partie.getJeu().retournerDeplacementsDeux());
        this.mettreAJourAnnulation();
    }

    /**
     * Applique un mouvement ou termine une partie entre ordinateur
     *
     * @param touche
     * @param codeTouche
     * @return
     */
    private boolean appliquerMouvement(String touche, KeyCode codeTouche) {
        if (this.interdireActionJoueur) {
            return false;
        }

        boolean application = false;

        // j1 humain
        if (typeJoueurUn.equals(JOUEUR_HUMAIN)) {
            application = appliquerMouvementHumainUn(touche);

            // humain vs humain
            if (typeJoueurDeux.equals(JOUEUR_HUMAIN)) {
                appliquerMouvementHumainDeux(touche);

                // humain vs ordinateur
            } else if (application) {
                appliquerMouvementOrdinateurDeux();
            }

            // ordinateur vs humain
        } else if (typeJoueurDeux.equals(JOUEUR_HUMAIN)) {
            application = appliquerMouvementHumainDeux(touche);

            if (application) {
                appliquerMouvementOrdinateurUn();
            }

            // ordinateur vs ordinateur
        } else // Touche pour effectuer une partie complette d'un seul coup
        {
            if (codeTouche == KeyCode.ENTER) {
                while (!partie.isTerminee()) {
                    appliquerMouvementOrdinateurUn();
                    appliquerMouvementOrdinateurDeux();
                }

            } else {
                appliquerMouvementOrdinateurUn();
                appliquerMouvementOrdinateurDeux();
            }
        }
        // mise a jours des conditions de victoire
        if (partie.isTerminee()) {
            mettreAJourEtat(PARTIE_TERMINEE);
        }

        // Mise à jour de la vue
        miseAJourVue();

        return application;
    }

    /**
     * Applique un mouvement pour le joueur 1 ordinateur
     */
    private void appliquerMouvementOrdinateurUn() {
        partie.getJeu().jouerUn();
    }

    /**
     * Applique un mouvement pour le joueur 2 ordinateur
     */
    private void appliquerMouvementOrdinateurDeux() {
        partie.getJeu().jouerDeux();
    }

    /**
     * Applique le mouvement chez le joueur 1 humain
     *
     * @param touche
     * @return vrai si le mouvement est valide
     */
    private boolean appliquerMouvementHumainUn(String touche) {
        boolean retour;

        switch (touche) {
            case TOUCHE_GAUCHE_UN:
                retour = partie.getJeu().jouerUn("GAUCHE");
                break;
            case TOUCHE_DROITE_UN:
                retour = partie.getJeu().jouerUn("DROITE");
                break;
            case TOUCHE_HAUT_UN:
                retour = partie.getJeu().jouerUn("HAUT");
                break;
            case TOUCHE_BAS_UN:
                retour = partie.getJeu().jouerUn("BAS");
                break;
            default:
                retour = false;
        }

        return retour;
    }

    /**
     * Applique le mouvement chez le joueur 1 humain
     *
     * @param touche
     * @return vrai si le mouvement est valide
     */
    private boolean appliquerMouvementHumainDeux(String touche) {
        boolean retour;

        switch (touche) {
            case TOUCHE_GAUCHE_DEUX:
                retour = partie.getJeu().jouerDeux("GAUCHE");
                break;
            case TOUCHE_DROITE_DEUX:
                retour = partie.getJeu().jouerDeux("DROITE");
                break;
            case TOUCHE_HAUT_DEUX:
                retour = partie.getJeu().jouerDeux("HAUT");
                break;
            case TOUCHE_BAS_DEUX:
                retour = partie.getJeu().jouerDeux("BAS");
                break;
            default:
                retour = false;
        }

        return retour;
    }

    /**
     * Initialise la GUI et change la scène du théâtre
     */
    public void demarerGUI() {
        // Creation de la scene et ajout au theatre
        Scene scene = vuePartie.initialiserScene();

        // Ajout de la gestion d'evenements clavier
        scene.setOnKeyPressed(this.gestionnaireClavier);

        // changement de scene du théâtre
        this.changerScene(getTheatre(), scene);
    }

    /**
     * Met à jours l'état de la partie et finis la partie si les conditions sont
     * remplies
     *
     * @param etat nouvel état de la partie
     */
    private void mettreAJourEtat(String etat) {
        vuePartie.setEtat(etat);
        if (this.getTheatre() != null && etat == PARTIE_TERMINEE) {
            finirPartie();
        }
    }

    /**
     * Méthode appelée à la fin de la partie. Ajoute la partie à la BDD, et
     * affiche la vue de fin de partie. Désactive le clavier et la souris.
     */
    private void finirPartie() {
        this.interdireActionJoueur = true;
        ControleurBDD.enregistrerPartieBDD(partie);
        this.vuePartie.afficherFinPartie(partie.getJeu().retournerEtatJeu()
                .retournerGagnant(), this.getTheatre());
    }

    /**
     * Méthode appelée par l'action du bouton sauvegarder
     */
    private void annulerJoueurUn() {
        if (this.interdireActionJoueur) {
            return;
        }

        this.partie.getJeu().annulerCoupUn();
        miseAJourVue();
    }

    /**
     * Méthode appelée par l'action du bouton sauvegarder
     */
    private void annulerJoueurDeux() {
        if (this.interdireActionJoueur) {
            return;
        }

        this.partie.getJeu().annulerCoupDeux();
        miseAJourVue();
    }

    /**
     * Appel la mise à jour de la vue dans le cas d'une annulation
     */
    private void mettreAJourAnnulation() {
        Jeu jeu = this.partie.getJeu();
        this.vuePartie.mettreAJourAnnulation(
                jeu.peutAnnulerCoupUn(),
                jeu.peutAnnulerCoupDeux(),
                jeu.retournerCompteurAnnulationUn(),
                jeu.retournerCompteurAnnulationDeux());
    }

    /**
     * Méthode appelée par l'action du bouton sauvegarder
     */
    private void sauvegarder() {
        ObjectOutputStream oos = null;
        try {
            // Ecriture de la partie dans le fichier
            FileOutputStream fichier = new FileOutputStream("partie.ser");
            oos = new ObjectOutputStream(fichier);
            oos.writeObject(partie);
            oos.flush();
        } catch (final IOException e) {

        } finally {
            // Fermeture du flux d'écriture
            try {
                if (oos != null) {
                    oos.flush();
                    oos.close();
                }
            } catch (final IOException exception) {

            }
        }
    }
}
