/**
 * Contient les éléments de vue de la grille de jeu, à savoir la grille en elle
 * même, l'arrière-plan et les vues d'annimation des cases.
 */
package vue.jeu.grille;
