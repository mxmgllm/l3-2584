/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue.jeu.grille;

/**
 * Représente l'arrière-plan
 *
 * @author emarquer
 */
public class VueArrierePlan extends VueCases {

    /**
     * Initialise les vues des cases en GUI
     */
    @Override
    public void initaliserVue() {
        super.initaliserVue();
        nettoyerCases();
    }

    /**
     * Met à jours le rang d'une case
     *
     * @param rang nouveau rang de la case
     * @param x coordonnée x de la grille
     * @param y coordonnée x de la grille
     */
    @Override
    protected void miseAJourCase(int rang, int x, int y) {
        this.vuesCases[x][y].setRang(rang);

        // Cache la case si elle est vide
        this.vuesCases[x][y].setVisible(true);
    }
}
