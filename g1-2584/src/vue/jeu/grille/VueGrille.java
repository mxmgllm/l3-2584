/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue.jeu.grille;

import javafx.event.ActionEvent;
import javafx.scene.layout.StackPane;
import modele.deplacement.Deplacements;
import vue.ParametresGUI;

/**
 * Représente une grille de jeu, composée d'un arrière-plan, d'une couche
 * immobile de cases, et d'une couche mobile de déplacements
 *
 * @author mg
 */
public class VueGrille extends StackPane implements ParametresGUI {

    private final VueArrierePlan arrierePlan;
    private final VueCases cases;
    private final VueDeplacements deplacements;

    /**
     * Crée une nouvelle vue de grille
     */
    public VueGrille() {
        // Creation des composants
        this.arrierePlan = new VueArrierePlan();
        this.deplacements = new VueDeplacements(this.arrierePlan);
        this.cases = new VueCases();

        // Lien des dimentions des cases et de l'arriere-plan
        this.deplacements.prefWidthProperty().bind(
                this.arrierePlan.widthProperty());

        // Active le gestionnaire de fin d'animation
        this.activerRetourFinAnnimation();

        // Lien de l'arrière-plan, des mouvements et des cases au parent
        this.getChildren().add(this.arrierePlan);
        this.getChildren().add(this.cases);
        this.getChildren().add(this.deplacements);
    }

    /**
     * Met à jour les cases avec le contenu de la grille
     *
     * @param grille
     */
    public void mettreAJour(Integer[][] grille) {
        this.cases.setCases(grille);
    }

    /**
     * Initialise les vues en GUI
     */
    public void initaliserVue() {
        // Initialisation de la taille minimale
        this.setMinWidth(TAILLE_CASE * 4);
        this.setMinHeight(this.getMinWidth());
        this.prefHeightProperty().bind(this.prefWidthProperty());

        // Initialisation de l'arriere plan
        this.arrierePlan.initaliserVue();

        // Initialisation des cases
        this.cases.initaliserVue();

        // Initialisation des mouvements
        this.deplacements.initaliserVue();

        // Affiche les cases
        this.afficherDeplacements(false);
    }

    /**
     * Initialise les vues des cases en GUI
     */
    public void miseAJourCases() {
        this.cases.miseAJourCases();
    }

    /**
     * @param afficher si {@code true}, affiche les animations et cache les
     * cases, sinon cache les animations et affiche les cases
     */
    private void afficherDeplacements(boolean afficher) {
        this.deplacements.setVisible(afficher);
        this.cases.setVisible(!afficher);
    }

    /**
     * Active un gestionnaire quand toutes les animations sont terminées. Ce
     * gestionnaire cache la vue de déplacement et affiche les cases.
     */
    private void activerRetourFinAnnimation() {
        this.deplacements.setGestionaireFinAnimations(
                (ActionEvent t) -> {
                    afficherDeplacements(false);
                });
    }

    /**
     * Met à jours les déplacement du joueur et les affiche.
     *
     * @param deplacements déplacements du joueur
     */
    public void mettreAJourDeplacements(Deplacements deplacements) {
        afficherDeplacements(false);
        if (this.deplacements.sontNouveauDeplacement(deplacements)) {
            afficherDeplacements(true);
            this.deplacements.setDeplacements(deplacements);
        }
    }
}
