/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue.jeu.grille;

import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import modele.deplacement.Apparition;
import vue.jeu.VueCase;

/**
 * Vue de l'apparition d'une case à un point donné
 *
 * @author emarquer
 */
class VueApparition implements VueDeplacement {

    private VueDeplacements vueDeplacements;
    private VueCase vueCase;
    private FadeTransition animation;
    private Apparition apparition;

    /**
     * Crée une vue d'apparition animée
     *
     * @param apparition apparition à partir de laquelle créer la vue
     * @param vueDeplacements vue des déplacements à lier à la nouvelle vue,
     * pour signaler la fin de l'annimation
     */
    public VueApparition(Apparition apparition, VueDeplacements vueDeplacements) {
        this.apparition = apparition;
        this.vueDeplacements = vueDeplacements;
        this.vueCase = new VueCase(this.apparition.getRang());

        // Initialisation de l'animation
        this.animation = new FadeTransition(TEMPS_APPARITION, vueCase);
        this.animation.setFromValue(0);
        this.animation.setToValue(1);
    }

    @Override
    public void jouer() {
        this.vueDeplacements.placerVueCase(this.vueCase,
                this.apparition.getX(), this.apparition.getY());
        this.animation.play();
    }

    @Override
    public void interrompreEtAchever() {
        this.animation.pause();
        this.nettoyer();
    }

    @Override
    public void nettoyer() {
        this.animation.setNode(null);
        this.vueCase = null;
        this.apparition = null;
        this.vueDeplacements = null;
        this.animation = null;
    }

    @Override
    public void gestionaireFin(EventHandler<ActionEvent> gestionaire) {
        this.animation.setOnFinished(gestionaire);
    }
}
