/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue.jeu.grille;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import modele.deplacement.Apparition;
import modele.deplacement.Deplacement;
import modele.deplacement.Fusion;
import modele.deplacement.Translation;
import vue.ParametresGUI;

/**
 * Vue de déplacement de case, ainsi que les méthodes nécessaires à l'analyse du
 * modèle de mouvement
 *
 * @author emarquer
 */
public interface VueDeplacement extends ParametresGUI {

    /**
     * Analyse un déplacement et crée la vue correspondante
     *
     * @param deplacement déplacement à analyser
     * @return vue correspondante au déplacement
     */
    public static VueDeplacement analyserDeplacement(
            Deplacement deplacement, VueDeplacements vueDeplacements) {

        // Crée la vue en fonction du type de deplacement
        if (deplacement instanceof Apparition) {
            return creerApparition((Apparition) deplacement, vueDeplacements);
        } else if (deplacement instanceof Translation) {
            return creerTranslation((Translation) deplacement, vueDeplacements);
        } else if (deplacement instanceof Fusion) {
            return creerFusion((Fusion) deplacement, vueDeplacements);
        }

        return null;
    }

    /**
     * Crée une vue de apparition à partir d'une apparition
     *
     * @param apparition apparition à analyser
     * @return vue de l'apparition
     */
    public static VueDeplacement creerApparition(
            Apparition apparition, VueDeplacements vueDeplacements) {
        return new VueApparition(apparition, vueDeplacements);
    }

    /**
     * Crée une vue de translation à partir d'une translation
     *
     * @param translation translation à analyser
     * @return vue de la translation
     */
    public static VueDeplacement creerTranslation(
            Translation translation, VueDeplacements vueDeplacements) {
        return new VueTranslation(translation, vueDeplacements);
    }

    /**
     * Crée une vue de fusion à partir d'une fusion
     *
     * @param fusion fusion à analyser
     * @return vue de la fusion
     */
    public static VueDeplacement creerFusion(
            Fusion fusion, VueDeplacements vueDeplacements) {
        return new VueFusion(fusion, vueDeplacements); // setOnFinished(value);
    }

    /**
     * Joue l'animation de déplacement
     */
    public void jouer();

    /**
     * Stoppe l'animation et la met à son point d'arrivée (irréversible, détruit
     * les références)
     */
    public void interrompreEtAchever();

    /**
     * Détruit les références (irréversible)
     */
    public void nettoyer();

    /**
     * Attribue un gestionnaire d'evenements pour quand l'animation se termine
     */
    public void gestionaireFin(EventHandler<ActionEvent> gestionaire);
}
