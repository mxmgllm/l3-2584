/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue.jeu.grille;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import modele.deplacement.Fusion;

/**
 * Vue de la fusion de deux cases, formée à partir du déplacement de deux cases
 * et de l'apparition d'une nouvelle case
 *
 * @author emarquer
 */
class VueFusion implements VueDeplacement {

    private VueTranslation vueTranslationParentUn, vueTranslationParentDeux;
    private VueApparition vueApparitionFusion;

    private volatile boolean translationParentUnFinie;
    private volatile boolean translationParentDeuxFinie;

    /**
     * Crée une vue d'apparition animée
     *
     * @param fusion fusion à partir de laquelle créer la vue
     * @param vueDeplacements vue des déplacements à lier à la nouvelle vue,
     * pour signaler la fin de l'annimation
     */
    public VueFusion(Fusion fusion, VueDeplacements vueDeplacements) {
        this.vueTranslationParentUn
                = new VueTranslation(
                        fusion.getDeplacementParentUn(),
                        vueDeplacements);
        this.vueTranslationParentDeux
                = new VueTranslation(
                        fusion.getDeplacementParentDeux(),
                        vueDeplacements);
        this.vueApparitionFusion
                = new VueApparition(
                        fusion.getApparitionFusion(),
                        vueDeplacements);

        // Lien des translations avec des gestionnaires d'évènements
        this.vueTranslationParentUn.gestionaireFin(
                (ActionEvent t) -> {
                    finTranslationUn();
                });
        this.vueTranslationParentDeux.gestionaireFin(
                (ActionEvent t) -> {
                    finTranslationDeux();
                });
    }

    /**
     * Notifie de la fin de la translation du parent un et tente de jouer
     * l'animation d'apparition de la case fusionnée
     */
    private void finTranslationUn() {
        this.translationParentUnFinie = true;
        this.finTranslation();
    }

    /**
     * Notifie de la fin de la translation du parent deux et tente de jouer
     * l'animation d'apparition de la case fusionnée
     */
    private void finTranslationDeux() {
        this.translationParentDeuxFinie = true;
        this.finTranslation();
    }

    /**
     * Joue l'apparition de la case fusionnée si les animation précédentes sont
     * finies
     */
    private void finTranslation() {
        if (this.translationParentUnFinie && this.translationParentDeuxFinie) {
            this.vueApparitionFusion.jouer();
        }
    }

    @Override
    public void jouer() {
        this.vueTranslationParentUn.jouer();
        this.vueTranslationParentDeux.jouer();
    }

    @Override
    public void interrompreEtAchever() {
        this.vueTranslationParentUn.interrompreEtAchever();
        this.vueTranslationParentDeux.interrompreEtAchever();
        this.vueApparitionFusion.interrompreEtAchever();
        this.nettoyer();
    }

    @Override
    public void nettoyer() {
        this.vueTranslationParentUn = null;
        this.vueTranslationParentDeux = null;
        this.vueApparitionFusion = null;
    }

    @Override
    public void gestionaireFin(EventHandler<ActionEvent> gestionaire) {
        this.vueApparitionFusion.gestionaireFin(gestionaire);
    }
}
