/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue.jeu.grille;

import javafx.scene.layout.GridPane;
import vue.ParametresGUI;
import vue.jeu.VueCase;

/**
 * Représente une grille contenant des cases
 */
public class VueCases extends GridPane implements ParametresGUI {

    private Integer[][] cases;
    protected VueCase[][] vuesCases;

    /**
     * Créé une nouvelle vue de cases
     */
    public VueCases() {
        cases = new Integer[TAILLE][TAILLE];
        this.vuesCases = new VueCase[TAILLE][TAILLE];
        this.prefWidthProperty().bindBidirectional(this.prefHeightProperty());
        this.setVgap(0);
        this.setHgap(0);
    }

    /**
     * Met a jours les cases
     *
     * @param grille nouvelles valeurs des cases
     */
    public void mettreAJour(Integer[][] grille) {
        setCases(grille);
    }

    /**
     * Met a jours les cases et leur vue
     *
     * @param cases nouvelles valeurs des cases
     */
    protected void setCases(Integer[][] cases) {
        this.cases = cases;

        if (this.getChildren().size() > 0) {
            this.miseAJourCases();
        }
    }

    /**
     * Initialise les vues des cases en GUI
     */
    public void initaliserVue() {
        //Initialisation de la taille minimale
        this.setMinWidth(TAILLE_CASE * 4);
        this.setMinHeight(this.getMinWidth());
        this.setWidth(this.getMinWidth());
        this.setHeight(this.getMinHeight());

        //Creation des cases
        for (int i = 0; i < cases.length; i++) {
            for (int j = 0; j < cases[i].length; j++) {
                creerCase(cases[i][j], i, j);
            }
        }
    }

    /**
     * Crée une case et l'ajoute à la grille
     *
     * @param rang rang de la case
     * @param x coordonnée x de la grille
     * @param y coordonnée x de la grille
     */
    protected void creerCase(Integer rang, int x, int y) {
        creerCase(x, y);

        if (rang != null) {
            miseAJourCase(rang, x, y);
        }
    }

    /**
     * Crée une case vide et l'ajoute à la grille
     *
     * @param x coordonnée x de la grille
     * @param y coordonnée x de la grille
     */
    protected void creerCase(int x, int y) {
        VueCase vueCase = new VueCase(
                CELLULE_VIDE,
                this.widthProperty().divide(4));

        this.placerVueCase(vueCase, x, y);
    }

    /**
     * Ajoute une vue de case à la grille
     *
     * @param vueCase case à ajouter
     * @param x coordonnée x de la grille
     * @param y coordonnée x de la grille
     */
    protected void placerVueCase(VueCase vueCase, int x, int y) {
        this.vuesCases[x][y] = vueCase;
        this.add(vueCase, y, x);
    }

    /**
     * @param x coordonnée x de la grille
     * @param y coordonnée x de la grille
     * @return vue de case aux coordonnées demandées
     */
    protected VueCase retournerVueCase(int x, int y) {
        return this.vuesCases[x][y];
    }

    /**
     * Met à jours le rang d'une case
     *
     * @param rang nouveau rang de la case
     * @param x coordonnée x de la grille
     * @param y coordonnée x de la grille
     */
    protected void miseAJourCase(int rang, int x, int y) {
        this.vuesCases[x][y].setRang(rang);

        // Cache la case si elle est vide
        this.vuesCases[x][y].setVisible(
                rang != CELLULE_VIDE);
    }

    /**
     * Met a jour la valeur des vues des cases en GUI
     */
    public void miseAJourCases() {
        for (int i = 0; i < cases.length; i++) {
            for (int j = 0; j < cases[i].length; j++) {
                miseAJourCase(cases[i][j], i, j);
            }
        }
    }

    /**
     * Remplace les vues des cases en GUI par des cases vides
     */
    protected void nettoyerCases() {
        for (int i = 0; i < cases.length; i++) {
            for (int j = 0; j < cases[i].length; j++) {
                miseAJourCase(CELLULE_VIDE, i, j);
            }
        }
    }
}
