/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue.jeu.grille;

import javafx.animation.TranslateTransition;
import javafx.beans.binding.DoubleBinding;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import modele.deplacement.Translation;
import vue.jeu.VueCase;

/**
 * Vue de la translation d'une case d'un point à un autre
 *
 * @author emarquer
 */
class VueTranslation implements VueDeplacement {

    private VueDeplacements vueDeplacements;
    private VueCase vueCase;
    private TranslateTransition animation;
    private Translation translation;

    /**
     * Crée une vue de translation animée
     *
     * @param translation translation à partir de laquelle créer la vue
     * @param vueDeplacements vue des déplacements à lier à la nouvelle vue,
     * pour signaler la fin de l'annimation
     */
    public VueTranslation(Translation translation, VueDeplacements vueDeplacements) {
        this.translation = translation;
        this.vueDeplacements = vueDeplacements;
        this.vueCase = new VueCase(this.translation.getRang());

        // recupération des movements necessaires
        int differenceX = this.translation.retournerDifferenceX();
        int differenceY = this.translation.retournerDifferenceY();
        int distance = Math.abs(differenceX + differenceY);

        // initialisation de l'animation
        this.animation = new TranslateTransition(
                TEMPS_TRANSLATION_UNE_CASE.multiply(distance), this.vueCase);

        // differenceX*largeurCase
        DoubleBinding translationX = this.vueDeplacements.retournerLargeurCase()
                .multiply(differenceY);

        // differenceY*hauteurCase
        DoubleBinding translationY = this.vueDeplacements.retournerHauteurCase()
                .multiply(differenceX);

        this.animation.byXProperty().bind(translationX);
        this.animation.byYProperty().bind(translationY);
    }

    @Override
    public void jouer() {
        this.vueDeplacements.placerVueCase(this.vueCase,
                this.translation.getXDepart(), this.translation.getYDepart());
        this.animation.play();
    }

    @Override
    public void interrompreEtAchever() {
        this.animation.pause();
        this.nettoyer();
    }

    @Override
    public void nettoyer() {
        this.animation.setNode(null);
        this.vueCase = null;
        this.translation = null;
        this.vueDeplacements = null;
        this.animation = null;
    }

    @Override
    public void gestionaireFin(EventHandler<ActionEvent> gestionaire) {
        this.animation.setOnFinished(gestionaire);
    }
}
