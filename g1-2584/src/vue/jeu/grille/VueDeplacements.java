/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue.jeu.grille;

import java.util.ArrayList;
import javafx.beans.binding.DoubleBinding;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import modele.deplacement.Deplacement;
import modele.deplacement.Deplacements;
import vue.jeu.VueCase;

/**
 * Représente l'ensemble des déplacements de cases d'une grille
 */
public class VueDeplacements extends VueCases {

    private Deplacements deplacements;
    private final ArrayList<VueDeplacement> vueDeplacements;
    private VueApparition vueApparition;
    private final VueArrierePlan arrierePlan;
    private EventHandler<ActionEvent> gestionaireFinAnimations;

    /**
     * nombre d'animations lancées mais pas encore terminées
     */
    private int animationsNonTerminees = 0;

    /**
     * Crée une nouvelle vue pour les déplacement de cases
     *
     * @param arrierePlan arrière-plan utilisé comme référence de taille pour
     * les déplacements
     */
    public VueDeplacements(VueArrierePlan arrierePlan) {
        super();
        this.vueDeplacements = new ArrayList<>();
        this.vuesCases = new VueCase[TAILLE][TAILLE];
        this.arrierePlan = arrierePlan;
    }

    /**
     * @return un lien vers la largeur calculée d'une case (à partir des
     * dimensions de l'arrière-plan)
     */
    public DoubleBinding retournerLargeurCase() {
        return this.arrierePlan.widthProperty().divide(TAILLE);
    }

    /**
     * @return un lien vers la hauteur calculée d'une case (à partir des
     * dimensions de l'arrière-plan)
     */
    public DoubleBinding retournerHauteurCase() {
        return this.arrierePlan.heightProperty().divide(TAILLE);
    }

    /**
     * Change les déplacements et lance les animations
     *
     * @param deplacements
     */
    public void setDeplacements(Deplacements deplacements) {
        this.deplacements = deplacements;
        this.nettoyerDeplacements();
        this.initialiserDeplacements();
    }

    /**
     * Initialise les vues des cases en GUI
     */
    @Override
    public void initaliserVue() {
        super.initaliserVue();
        nettoyerCases();
    }

    /**
     * Initialise les déplacements des cases en GUI
     */
    private void initialiserDeplacements() {
        // Protection contre les valeurs nules
        if (this.deplacements == null) {
            return;
        }

        // Transforme les déplacements en animations correspondantes
        for (Deplacement deplacement : this.deplacements) {
            this.creerDeplacement(deplacement);
        }

        // Active le gestionnaire de fin d'annimations
        activerRetourFinAnnimation();

        // Démare tous les déplacements
        this.jouer();
    }

    /**
     * Créé une vue pour le déplacement passé en paramètre
     *
     * @param deplacement
     */
    private void creerDeplacement(Deplacement deplacement) {
        VueDeplacement vueDeplacement
                = VueDeplacement.analyserDeplacement(deplacement, this);

        if (vueDeplacement instanceof VueApparition) {
            this.vueApparition = (VueApparition) vueDeplacement;
        } else {
            this.vueDeplacements.add(vueDeplacement);
        }

        // Augmente le nombre d'animations
        this.animationsNonTerminees++;
    }

    /**
     * Interrompt les déplacements en cours (si il y en a) et détruit les
     * références
     */
    private void nettoyerDeplacements() {
        for (VueDeplacement deplacement : this.vueDeplacements) {
            deplacement.interrompreEtAchever();
        }

        this.vueDeplacements.clear();
        this.getChildren().clear();
        this.initaliserVue();
        this.animationsNonTerminees = 0;
    }

    /**
     * Démarre l'ensemble des animations
     */
    private void jouer() {
        for (VueDeplacement deplacement : this.vueDeplacements) {
            deplacement.jouer();
        }
    }

    /**
     * Active un gestionnaire quand toutes les animations sont terminées.
     *
     * @param gestionaireFinAnimations gestionnaire à appeler à la fin des
     * animations
     */
    public void setGestionaireFinAnimations(
            EventHandler<ActionEvent> gestionaireFinAnimations) {
        this.gestionaireFinAnimations = gestionaireFinAnimations;
    }

    /**
     * Active un gestionnaire quand toutes les animations sont terminées.
     */
    private void activerRetourFinAnnimation() {
        // Gestionnaire d'évenement pour chaque animation
        EventHandler<ActionEvent> gestionaireFinAnimation
                = (ActionEvent t) -> {
                    retourFinAnimation(t);
                };

        for (VueDeplacement deplacement : this.vueDeplacements) {
            deplacement.gestionaireFin(gestionaireFinAnimation);
        }
    }

    /**
     * Méthode appelée à la fin de chaque animation
     *
     * @param t
     */
    private void retourFinAnimation(ActionEvent t) {
        this.animationsNonTerminees--;

        /* Une fois toutes les animations sauf l'apparition de nouvelle case
         * terminées, joue l'apparition de nouvelle case
         */
        if (this.animationsNonTerminees == 1 && vueApparition != null) {
            vueApparition.jouer();
            vueApparition = null;
        }

        // Une fois toutes les animations finies, gère la fin des animations
        if (this.animationsNonTerminees < 1) {
            this.gestionaireFinAnimations.handle(t);
        }
    }

    /**
     * Vérifie si les déplacements sont nouveaux
     *
     * @param deplacements déplacements à tester
     * @return vrai si les déplacement ne sont ni {@code null}, ni vides, ni
     * égaux aux derniers déplacements
     */
    public boolean sontNouveauDeplacement(Deplacements deplacements) {
        return deplacements != null && !deplacements.isEmpty()
                && !deplacements.equals(this.deplacements);
    }
}
