/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue.jeu;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import vue.ParametresGUI;

/**
 * Bouton pour annuler un coup, avec une mémoire du nombre d'annulations encore
 * disponibles
 *
 * @author darven
 */
public class VueAnnuler extends Button implements ParametresGUI {

    private int annulerRestant = 0;

    /**
     * Créé une nouvelle vue d'annulation
     */
    public VueAnnuler() {
        super();
        this.setAnnulerRestant(NOMBRE_ANNULATION);
        this.setDisable(true);
        this.setTooltip(new Tooltip(TEXTE_FLOTANT_BOUTON_ANNULER));
    }

    /**
     * @param annulerRestant le nombre d'annulation réstantes
     */
    public void setAnnulerRestant(int annulerRestant) {
        this.annulerRestant = annulerRestant;
        this.setText(String.format(TEXTE_BOUTON_ANNULER, this.annulerRestant));

        // Desactive le bouton si moins de 1 annulation est disponible
        if (this.annulerRestant < 1) {
            this.setDisable(true);
        } else {
            this.setDisable(false);
        }
    }

    /**
     * Initialise la gestion d'evenement du bouton annuler
     *
     * @param gestionnaireJoueur gestionnaire d'evennements pour l'annulation de
     * coup
     */
    public void initialiserAction(EventHandler<ActionEvent> gestionnaireJoueur) {
        this.setOnAction(gestionnaireJoueur);
    }

    /**
     * Met a jour la vue d'annulation de coup
     *
     * @param peutAnnulerCoup vrai si le joueur peut annuler un coup
     * @param compteurAnnulation nombre d'annulations restantes
     */
    public void mettreAJour(boolean peutAnnulerCoup, int compteurAnnulation) {
        this.setAnnulerRestant(compteurAnnulation);
        this.setDisable(!peutAnnulerCoup);
    }
}
