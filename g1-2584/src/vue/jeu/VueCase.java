/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue.jeu;

import javafx.beans.value.ObservableDoubleValue;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import vue.ParametresGUI;

/**
 * Vue d'une case, affichant sa valeur et un fond dépendant de son rang
 *
 * @author darven
 */
public class VueCase extends StackPane implements ParametresGUI {

    private Integer rang;
    private Rectangle arierePlan;
    private Label texte;

    /**
     * Créé une case contrainte pour être carrée si posssible
     *
     * @param rang rang de la case
     * @param tailleCote taille du côté de la case
     */
    public VueCase(Integer rang, ObservableDoubleValue tailleCote) {
        this(rang);

        // Liens de la taille des cotes de la case (pour en faire un carre)
        this.setMinSize(TAILLE_CASE, TAILLE_CASE);
        this.prefWidthProperty().bind(tailleCote);
        this.prefHeightProperty().bindBidirectional(this.prefWidthProperty());
    }

    /**
     * Créé une case
     *
     * @param rang rang de la case
     */
    public VueCase(Integer rang) {
        this.setCenterShape(true);

        // Creation de l'ariere-plan
        this.arierePlan = new Rectangle();
        this.getChildren().add(this.arierePlan);

        // Initialisation des angles de l'ariere-plan
        this.arierePlan.arcWidthProperty().set(TAILLE_ARRONDI_CASE);
        this.arierePlan.arcHeightProperty().bind(
                this.arierePlan.arcWidthProperty());

        // Lien de l'ariere-plan au conteneur
        this.arierePlan.widthProperty().bind(this.widthProperty().add(-8));
        this.arierePlan.heightProperty().bind(this.heightProperty().add(-8));

        // Creation du texte
        this.texte = new Label();
        this.texte.setTextFill(COULEUR_TEXTE_CASES);
        this.texte.setFont(Font.font(TAILLE_TEXTE_CASES));
        this.texte.setPadding(new Insets(10));

        this.setRang(rang);
        this.getChildren().add(this.texte);
    }

    /**
     * @return rang dans la suite
     */
    public Integer getRang() {
        return rang;
    }

    /**
     * @param rang nouveau rang dans la suite
     */
    public void setRang(Integer rang) {
        this.rang = rang;
        this.texte.setText(texteRang(this.rang));
        this.miseAJourCouleurs();
    }

    /**
     * Met a jours la couleur d'ariere-plan en fonction du rang
     */
    private void miseAJourCouleurs() {
        if (this.rang < 0) {
            this.arierePlan.setFill(COULEUR_CASE_VIDE);
        } else {
            this.arierePlan.setFill(COULEURS_CASES[rang]);
        }
    }

    /**
     * Renvoie le texte correspondant au rang passe en parametre
     *
     * @param rang rang dans la suite
     * @return texte correspondant au rang
     */
    public static String texteRang(int rang) {
        if (rang < 0) {
            return "";
        } else if (rang >= SUITE.length) {
            return "Rang invalide";
        } else {
            return SUITE[rang] + "";
        }
    }
}
