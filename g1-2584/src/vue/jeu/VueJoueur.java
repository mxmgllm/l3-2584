/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue.jeu;

import vue.jeu.grille.VueGrille;
import javafx.beans.binding.DoubleBinding;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.layout.GridPane;
import modele.deplacement.Deplacements;
import vue.ParametresGUI;

/**
 * Vue du joueur, contenant une vue de grille de jeu, un bouton pour annuler les
 * coups, et un affichage du nom du joueur, du nombre de coups joués et du score
 *
 * @author mg
 */
public class VueJoueur implements ParametresGUI {

    private String score;
    private int nombreCoups;

    private VueGrille vueGrille;
    private VueAnnuler vueAnnuler;
    private final EventHandler<ActionEvent> gestionnaireAnnulation;

    private String typeJoueur;
    private Label nomJoueur;
    private Label vueScore;
    private Label vueNombreCoups;

    /**
     * Crée une nouvelle vue de joueur
     *
     * @param typeJoueur type de joueur du joueur deux
     * @param gestionnaireAnnulation gestionnaire d'évènements pour l'annulation
     * de coup du joueur un
     */
    public VueJoueur(
            String typeJoueur,
            EventHandler<ActionEvent> gestionnaireAnnulation) {
        this.typeJoueur = typeJoueur;
        this.gestionnaireAnnulation = gestionnaireAnnulation;

        score = new String();
        vueGrille = new VueGrille();

        this.nombreCoups = 0;
    }

    /**
     *
     *
     * @param score nouveau score du joueur
     * @param nombreCoups nouveau nombre de coups du joueurs
     * @param grille nouvelle grille du joueur
     */
    public void mettreAJour(String score, int nombreCoups, Integer[][] grille) {
        setScore(score);
        setNombreCoups(nombreCoups);
        vueGrille.mettreAJour(grille);
    }

    /**
     * @param score le nouveau score
     */
    public void setScore(String score) {
        this.score = score;
        if (this.vueScore != null) {
            this.vueScore.setText(String.format(TEXTE_SCORE, this.score));
        }
    }

    /**
     * @param nombreCoups le nouveau nombre de coups
     */
    public void setNombreCoups(int nombreCoups) {
        this.nombreCoups = nombreCoups;
        if (this.vueNombreCoups != null) {
            this.vueNombreCoups.setText(
                    String.format(TEXTE_NOMBRE_COUPS, this.nombreCoups));
        }
    }

    /**
     * Initialise la vue en GUI
     *
     * @param grille grille graphique dans laquelle ajouter le joueur
     * @param estJoueurUn {@code true} si le joueur est le joueur un,
     * {@code false} si le joueur est le joueur deux
     */
    public void initialiserVue(GridPane grille,
            boolean estJoueurUn) {
        // Definition de la colonne concernee
        int colonne = estJoueurUn ? 0 : 1;
        DoubleBinding largeurColonne = grille.widthProperty().divide(2);

        // initialisation de la GUI du nom du joueur
        initialiserNomJoueur(grille, largeurColonne, colonne);

        // initialisation de la GUI du score
        initialiserScore(grille, largeurColonne, colonne);

        // initialisation de la GUI du nombre de coups
        initialiserNombreCoups(grille, largeurColonne, colonne);

        // initialisation de la GUI de l'annulation, si le joueur est humain
        if (typeJoueur == JOUEUR_HUMAIN) {
            initialiserAnnulation(grille, largeurColonne, colonne);
        }

        // separation entre la grille et l'annulation
        initialiserSeparateur(grille, largeurColonne, colonne);

        // initialisation de la GUI de la grille
        initialiserGrille(grille, largeurColonne, colonne);
    }

    /**
     * Initialise le nom du joueur
     *
     * @param grille grille graphique dans laquelle ajouter le joueur
     * @param largeurColonne largeur de la colonne
     * @param colonne numéro de la colonne
     */
    private void initialiserNomJoueur(
            GridPane grille,
            DoubleBinding largeurColonne,
            int colonne) {
        this.vueScore = new Label(String.format(TEXTE_SCORE, this.score));
        this.vueScore.prefWidthProperty().bind(largeurColonne);
        this.vueScore.setAlignment(Pos.CENTER);
        grille.add(this.vueScore, colonne, 1);
    }

    /**
     * Initialise le score
     *
     * @param grille grille graphique dans laquelle ajouter le joueur
     * @param largeurColonne largeur de la colonne
     * @param colonne numéro de la colonne
     */
    private void initialiserScore(
            GridPane grille,
            DoubleBinding largeurColonne,
            int colonne) {
        this.nomJoueur = new Label(this.nomJoueur(typeJoueur));
        this.nomJoueur.prefWidthProperty().bind(largeurColonne);
        this.nomJoueur.setAlignment(Pos.CENTER);
        grille.add(this.nomJoueur, colonne, 0);
    }

    /**
     * Initialise le nombre de coups
     *
     * @param grille grille graphique dans laquelle ajouter le joueur
     * @param largeurColonne largeur de la colonne
     * @param colonne numéro de la colonne
     */
    private void initialiserNombreCoups(
            GridPane grille,
            DoubleBinding largeurColonne,
            int colonne) {
        this.vueNombreCoups = new Label(
                String.format(TEXTE_NOMBRE_COUPS, this.nombreCoups));
        this.vueNombreCoups.prefWidthProperty().bind(largeurColonne);
        this.vueNombreCoups.setAlignment(Pos.CENTER);
        grille.add(this.vueNombreCoups, colonne, 2);
    }

    /**
     * Initialise l'annulation
     *
     * @param grille grille graphique dans laquelle ajouter le joueur
     * @param largeurColonne largeur de la colonne
     * @param colonne numéro de la colonne
     */
    private void initialiserAnnulation(
            GridPane grille,
            DoubleBinding largeurColonne,
            int colonne) {
        this.vueAnnuler = new VueAnnuler();
        this.vueAnnuler.prefWidthProperty().bind(largeurColonne);
        if (gestionnaireAnnulation != null) {
            this.vueAnnuler.setOnAction(gestionnaireAnnulation);
        }
        grille.add(this.vueAnnuler, colonne, 3);
    }

    /**
     * Initialise le séparateur
     *
     * @param grille grille graphique dans laquelle ajouter le joueur
     * @param largeurColonne largeur de la colonne
     * @param colonne numéro de la colonne
     */
    private void initialiserSeparateur(
            GridPane grille,
            DoubleBinding largeurColonne,
            int colonne) {
        Separator separation = new Separator(Orientation.HORIZONTAL);
        separation.prefWidthProperty().bind(largeurColonne);
        grille.add(separation, colonne, 4);
    }

    /**
     * Initialise la grille
     *
     * @param grille grille graphique dans laquelle ajouter le joueur
     * @param largeurColonne largeur de la colonne
     * @param colonne numéro de la colonne
     */
    private void initialiserGrille(
            GridPane grille,
            DoubleBinding largeurColonne,
            int colonne) {
        this.vueGrille.initaliserVue();
        this.vueGrille.prefWidthProperty().bind(largeurColonne);
        grille.add(this.vueGrille, colonne, 5);
    }

    /**
     * Met a jour la vue d'annulation de coup
     *
     * @param peutAnnulerCoup vrai si le joueur peut annuler un coup
     * @param compteurAnnulation nombre d'annulations restantes
     */
    public void mettreAJourAnnulation(boolean peutAnnulerCoup, int compteurAnnulation) {
        if (vueAnnuler != null) {
            this.vueAnnuler.mettreAJour(peutAnnulerCoup, compteurAnnulation);
        }
    }

    /**
     * Met à jours les déplacement du joueur et les affiche.
     *
     * @param deplacements déplacements du joueur
     */
    public void mettreAJourDeplacements(Deplacements deplacements) {
        this.vueGrille.mettreAJourDeplacements(deplacements);
    }

    /**
     * @param typeJoueur type du joueur dont on veut le nom
     * @return nom correspondant au type de joueur
     */
    public static String nomJoueur(String typeJoueur) {
        switch (typeJoueur) {
            default:
            case JOUEUR_HUMAIN:
                return TEXTE_NOM_JOUEUR_HUMAIN;
            case JOUEUR_ORDINATEUR:
                return TEXTE_NOM_JOUEUR_ORDINATEUR;
            case JOUEUR_IA:
                return TEXTE_NOM_JOUEUR_IA;
        }
    }
}
