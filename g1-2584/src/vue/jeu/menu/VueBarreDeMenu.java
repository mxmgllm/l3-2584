/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue.jeu.menu;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Menu;
import javafx.scene.layout.VBox;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import vue.ParametresGUI;

/**
 * Vue de la barre de menu, permettant d'afficher les commandes et la suite,
 * ainsi que d'accéder aux commandes de nouvelle partie, de sauvegarde, de
 * chargement, d'affichage des parties precedentes, et d'arret du programme.
 *
 * @author darven
 */
public class VueBarreDeMenu extends VBox implements ParametresGUI {

    private MenuBar vueMenu;
    private Menu vueMenuJeu;
    private Menu vueMenuAide;
    private MenuItem vueNouvellePartie;
    private MenuItem vueSauvegarde;
    private MenuItem vueCharger;
    private MenuItem vuePartiesPrecedentes;
    private MenuItem vueQuitter;
    private CheckMenuItem vueAfficherControles;
    private CheckMenuItem vueAfficherSuite;

    private VueSuite vueSuite;
    private VueControles vueControles;

    /**
     * Crée un menu contenant deux sous-menus : le menu d'aide; le menu de jeu.
     *
     * @param typeJoueurUn type de joueur du joueur un
     * @param typeJoueurDeux type de joueur du joueur deux
     * @param gestionnaireNouvellePartie gestionnaire d'évènements pour le
     * lancement de partie
     * @param gestionnaireChargerPartie gestionnaire d'évènements pour le
     * chargement de partie
     * @param gestionnaireSauvegarde gestionnaire d'évènements pour la
     * sauvegarde de partie
     * @param gestionnairePartiesPrecedentes gestionnaire d'évènements pour
     * afficher les parties précédentes
     * @param gestionnaireQuitter gestionnaire d'évènements pour quitter le jeu
     */
    public VueBarreDeMenu(
            String typeJoueurUn,
            String typeJoueurDeux,
            EventHandler<ActionEvent> gestionnaireNouvellePartie,
            EventHandler<ActionEvent> gestionnaireChargerPartie,
            EventHandler<ActionEvent> gestionnaireSauvegarde,
            EventHandler<ActionEvent> gestionnairePartiesPrecedentes,
            EventHandler<ActionEvent> gestionnaireQuitter) {
        this.vueMenu = new MenuBar();

        // Creation du menu de jeu
        this.initialiserMenuJeu(
                gestionnaireNouvellePartie,
                gestionnaireChargerPartie,
                gestionnaireSauvegarde,
                gestionnairePartiesPrecedentes,
                gestionnaireQuitter);
        this.vueMenu.getMenus().add(vueMenuJeu);

        // Creation du menu d'aide
        this.initialiserMenuAide();
        this.vueMenu.getMenus().add(vueMenuAide);

        // ajout du menu au conteneur
        this.getChildren().add(vueMenu);

        // Creation de la vue de la suite de Fibonacci
        this.vueSuite = new VueSuite();

        // Creation de la vue des controles des joueurs
        this.vueControles = new VueControles(typeJoueurUn, typeJoueurDeux);
    }

    /**
     * Crée un menu contenant deux sous-menus : le menu d'aide; le menu de jeu.
     *
     * @param gestionnaireNouvellePartie gestionnaire d'évènements pour le
     * lancement de partie
     * @param gestionnaireChargerPartie gestionnaire d'évènements pour le
     * chargement de partie
     * @param gestionnairePartiesPrecedentes gestionnaire d'évènements pour
     * afficher les parties précédentes
     * @param gestionnaireQuitter gestionnaire d'évènements pour quitter le jeu
     */
    public VueBarreDeMenu(
            EventHandler<ActionEvent> gestionnaireNouvellePartie,
            EventHandler<ActionEvent> gestionnaireChargerPartie,
            EventHandler<ActionEvent> gestionnairePartiesPrecedentes,
            EventHandler<ActionEvent> gestionnaireQuitter) {
        this.vueMenu = new MenuBar();

        // Creation du menu de jeu
        this.initialiserMenuJeu(
                gestionnaireNouvellePartie,
                gestionnaireChargerPartie,
                gestionnairePartiesPrecedentes,
                gestionnaireQuitter);
        this.vueMenu.getMenus().add(vueMenuJeu);

        // ajout du menu au conteneur
        this.getChildren().add(vueMenu);
    }

    /**
     * Enleve la vue de la suite de Fibonacci du dessous du menu, si elle y est
     */
    private void cacherSuite() {
        if (this.getChildren().contains(this.vueSuite)) {
            this.getChildren().remove(this.vueSuite);
        }
    }

    /**
     * Ajoute la vue de la suite de Fibonacci au dessous du menu, si elle n'y
     * est pas encore, et en dessus des controles, si ils y sont
     */
    private void afficherSuite() {
        if (!this.getChildren().contains(this.vueSuite)) {
            /* Ajout de la vue de la suite avant les controles si ils sont
             * presents
             */
            if (this.getChildren().contains(this.vueControles)) {
                this.getChildren().add(
                        this.getChildren().indexOf(this.vueControles),
                        this.vueSuite);

                /* Ajout de la vue de la suite a la fin si les controles ne sont pas
             * presents 
                 */
            } else {
                this.getChildren().add(this.vueSuite);
            }
        }
    }

    /**
     * Enleve la vue des controles des joueurs du dessous du menu, si elle y est
     */
    private void cacherControles() {
        if (this.getChildren().contains(this.vueControles)) {
            this.getChildren().remove(this.vueControles);
        }
    }

    /**
     * Ajoute la vue des controles des joueurs au dessous du menu, si elle n'y
     * est pas encore
     */
    private void afficherControles() {
        if (!this.getChildren().contains(this.vueControles)) {
            this.getChildren().add(this.vueControles);
        }
    }

    /**
     * Crée le menu de jeu et ses éléments : afficher les contrôles; afficher la
     * suite de Fibonacci.
     */
    private void initialiserMenuAide() {
        // Creation du menu de jeu
        this.vueMenuAide = new Menu("Aide");

        // Creation du bouton d'affichage des indications de controles
        this.vueAfficherControles
                = new CheckMenuItem(TEXTE_BOUTON_AFFICHER_CONTROLES);
        this.vueAfficherControles.setOnAction(
                (final ActionEvent evenement) -> {
                    miseAJourControles();
                });

        // Creation du bouton d'affichage de la suite
        this.vueAfficherSuite
                = new CheckMenuItem(TEXTE_BOUTON_AFFICHER_SUITE);
        this.vueAfficherSuite.setOnAction(
                (final ActionEvent evenement) -> {
                    miseAJourSuite();
                });

        // Ajout des elements au menu
        this.vueMenuAide.getItems().addAll(
                vueAfficherSuite,
                vueAfficherControles);
    }

    /**
     * Crée le menu de jeu et ses éléments : commencer une nouvelle partie;
     * sauvegarder; charger; afficher les anciennes parties; quitter;
     *
     * @param gestionNouvellePartie gestionnaire d'évènements pour le lancement
     * de partie
     * @param gestionChargerPartie gestionnaire d'évènements pour le chargement
     * de partie
     * @param gestionSauvegarde gestionnaire d'évènements pour la sauvegarde de
     * partie
     * @param gestionPartiesPrecedentes gestionnaire d'évènements pour afficher
     * les parties précédentes
     * @param gestionQuitter gestionnaire d'évènements pour quitter le jeu
     */
    private void initialiserMenuJeu(
            EventHandler<ActionEvent> gestionNouvellePartie,
            EventHandler<ActionEvent> gestionChargerPartie,
            EventHandler<ActionEvent> gestionSauvegarde,
            EventHandler<ActionEvent> gestionPartiesPrecedentes,
            EventHandler<ActionEvent> gestionQuitter) {

        // Creation du menu de jeu
        this.initialiserMenuJeu(gestionNouvellePartie, gestionChargerPartie, gestionPartiesPrecedentes, gestionQuitter);

        // Creation du bouton de sauvegarde
        this.vueSauvegarde = new MenuItem(TEXTE_BOUTON_SAUVEGARDE);
        this.vueSauvegarde.setOnAction(gestionSauvegarde);

        // Ajout du bouton de sauvegarde au menu
        this.vueMenuJeu.getItems().add(2, vueSauvegarde);
    }

    /**
     * Crée le menu de jeu et ses éléments : commencer une nouvelle partie;
     * charger; afficher les anciennes parties; quitter;
     *
     * @param gestionnaireNouvellePartie gestionnaire d'évènements pour le
     * lancement de partie
     * @param gestionnaireChargerPartie gestionnaire d'évènements pour le
     * chargement de partie
     * @param gestionnairePartiesPrecedentes gestionnaire d'évènements pour
     * afficher les parties précédentes
     * @param gestionnaireQuitter gestionnaire d'évènements pour quitter le jeu
     */
    private void initialiserMenuJeu(
            EventHandler<ActionEvent> gestionnaireNouvellePartie,
            EventHandler<ActionEvent> gestionnaireChargerPartie,
            EventHandler<ActionEvent> gestionnairePartiesPrecedentes,
            EventHandler<ActionEvent> gestionnaireQuitter) {

        // Creation du menu de jeu
        this.vueMenuJeu = new Menu("Jeu");

        // Creation du bouton de nouvelle partie
        this.vueNouvellePartie = new MenuItem(TEXTE_BOUTON_NOUVELLE_PARTIE);
        this.vueNouvellePartie.setOnAction(gestionnaireNouvellePartie);

        // Creation du bouton de chargement
        this.vueCharger = new MenuItem(TEXTE_BOUTON_CHARGER);
        this.vueCharger.setOnAction(gestionnaireChargerPartie);

        // Creation du bouton d'affichage des parties précédentes
        this.vuePartiesPrecedentes
                = new MenuItem(TEXTE_BOUTON_PARTIES_PRECEDENTES);
        this.vuePartiesPrecedentes.setOnAction(gestionnairePartiesPrecedentes);

        // Creation du bouton quitter
        this.vueQuitter = new MenuItem(TEXTE_BOUTON_QUITTER);
        this.vueQuitter.setOnAction(gestionnaireQuitter);

        // Ajout des elements au menu
        this.vueMenuJeu.getItems().addAll(
                vueNouvellePartie,
                vueCharger,
                vuePartiesPrecedentes,
                vueQuitter);
    }

    /**
     * Methode appelee par l'action du menu d'affichage de la suite
     */
    private void miseAJourSuite() {
        if (this.vueAfficherSuite.isSelected()) {
            this.afficherSuite();
        } else {
            this.cacherSuite();
        }
    }

    /**
     * Methode appelee par l'action du menu d'affichage des controles
     */
    private void miseAJourControles() {
        if (this.vueAfficherControles.isSelected()) {
            this.afficherControles();
        } else {
            this.cacherControles();
        }
    }
}
