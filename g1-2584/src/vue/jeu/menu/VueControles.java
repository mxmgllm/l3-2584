/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue.jeu.menu;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import vue.ParametresGUI;

/**
 * Vue des controles des joueurs, affiche les controles des joueurs derière
 * leurs nom, en precisant si ils sont contoles ou non par l'ordinateur ou l'IA
 *
 * @author emarquer
 */
class VueControles extends GridPane implements ParametresGUI {

    private Label vueControlesJoueurUn;
    private Label vueControlesJoueurDeux;

    /**
     * Cree une nouvelle vue des controles des joueurs
     *
     * @param typeJoueurUn type de joueur du joueur un
     * @param typeJoueurDeux type de joueur du joueur deux
     */
    public VueControles(String typeJoueurUn, String typeJoueurDeux) {
        // Initialisation du conteneur

        // Creation des textes des joueurs
        this.vueControlesJoueurUn = creerTexteControles(
                true, typeJoueurUn);
        this.vueControlesJoueurDeux = creerTexteControles(
                false, typeJoueurDeux);

        this.add(this.vueControlesJoueurUn, 0, 0);
        this.add(this.vueControlesJoueurDeux, 1, 0);
    }

    /**
     * Cree un texte en fonction du type de joueur et de son numero
     *
     * @param estJoueurUn vrai si ne joueur est joueur un, faux si il est joueur
     * deux
     * @param typeJoueur type de joueur du joueur
     * @return un {@code Text} contenant le nom du joueur et ses controles
     */
    private Label creerTexteControles(boolean estJoueurUn, String typeJoueur) {
        String controlesJoueur;

        switch (typeJoueur) {
            default:
            case JOUEUR_HUMAIN:
                controlesJoueur = estJoueurUn
                        ? TEXTE_TOUCHES_UN : TEXTE_TOUCHES_DEUX;
                break;
            case JOUEUR_ORDINATEUR:
                controlesJoueur = TEXTE_TOUCHES_ORDINATEUR;
                break;
            case JOUEUR_IA:
                controlesJoueur = TEXTE_TOUCHES_IA;
                break;
        }

        // Cree le nom du joueur et y ajoute les controles
        Label texte = new Label(String.format(
                estJoueurUn ? TEXTE_NOM_UN : TEXTE_NOM_DEUX,
                controlesJoueur));

        // Active le retour a la ligne du texte
        texte.setWrapText(true);
        texte.prefWidthProperty().bind(this.widthProperty().divide(2));
        texte.setPadding(new Insets(5, 5, 5, 5));

        return texte;
    }
}
