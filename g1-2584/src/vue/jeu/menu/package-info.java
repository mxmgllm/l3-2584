/**
 * Contient les éléments de vue de la barre de menu, ainsi que des éléments
 * qu'elle permet d'afficher (liste des touches des joueurs, et vue de la suite).
 */
package vue.jeu.menu;
