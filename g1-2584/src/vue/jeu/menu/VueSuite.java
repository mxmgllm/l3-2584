/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue.jeu.menu;

import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.layout.FlowPane;
import vue.ParametresGUI;
import vue.jeu.VueCase;

/**
 * Suite de Fibonacci
 *
 * @author darven
 */
public class VueSuite extends FlowPane implements ParametresGUI {

    public VueSuite() {
        super(Orientation.HORIZONTAL);

        // Ajoute tous les termas de la suite
        this.getChildren().add(new VueCase(0));
        for (int i = 0; i < SUITE.length; i++) {
            this.getChildren().add(new VueCase(i));
        }

        // Centre l'affichage
        this.setAlignment(Pos.CENTER);
    }

}
