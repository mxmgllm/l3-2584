/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue.jeu;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.layout.GridPane;
import modele.deplacement.Deplacements;
import vue.ParametresGUI;

/**
 * Vue de la partie en cours, contenant une vue de joueur par joueur.
 *
 * @author darven
 */
public class VueJeu extends GridPane implements ParametresGUI {

    private VueJoueur vueJoueurUn;
    private VueJoueur vueJoueurDeux;

    /**
     * Crée une nouvelle vue de jeu
     *
     * @param typeJoueurUn type de joueur du joueur un
     * @param typeJoueurDeux type de joueur du joueur deux
     * @param gestionnaireAnnulationUn gestionnaire d'évènements pour
     * l'annulation de coup du joueur un
     * @param gestionnaireAnnulationDeux gestionnaire d'évènements pour
     * l'annulation de coup du joueur deux
     */
    public VueJeu(
            String typeJoueurUn,
            String typeJoueurDeux,
            EventHandler<ActionEvent> gestionnaireAnnulationUn,
            EventHandler<ActionEvent> gestionnaireAnnulationDeux) {

        vueJoueurUn = new VueJoueur(
                typeJoueurUn,
                gestionnaireAnnulationUn);
        vueJoueurDeux = new VueJoueur(
                typeJoueurDeux,
                gestionnaireAnnulationDeux);

        this.setPadding(new Insets(10));
        this.hgapProperty().set(this.getPadding().getBottom());
        this.vgapProperty().bind(this.hgapProperty());
    }

    /**
     * Met à jour la vue
     *
     * @param scoreUn nouveau score du joueur un
     * @param scoreDeux nouveau score du joueur deux
     * @param nombreCoupsUn nouveau nombre de coup du joueur un
     * @param nombreCoupsDeux nouveau nombre de coup du joueur deux
     * @param grilleUn nouvelle grille du joueur un
     * @param grilleDeux nouvelle grille du joueur deux
     */
    public void miseAJour(Integer scoreUn, Integer scoreDeux,
            Integer nombreCoupsUn, Integer nombreCoupsDeux,
            Integer[][] grilleUn, Integer[][] grilleDeux) {
        vueJoueurUn.mettreAJour(scoreUn.toString(), nombreCoupsUn, grilleUn);
        vueJoueurDeux.mettreAJour(scoreDeux.toString(), nombreCoupsDeux, grilleDeux);
    }

    /**
     * Initialise la vue en GUI
     */
    public void initaliserVue() {
        // initialisation de la GUI du joueur un
        this.vueJoueurUn.initialiserVue(this, true);

        // initialisation de la GUI du joueur deux
        this.vueJoueurDeux.initialiserVue(this, false);
    }

    /**
     * Met a jour la vue d'annulation de coup des deux joueurs
     *
     * @param peutAnnulerCoupUn vrai si le joueur un peut annuler un coup
     * @param compteurAnnulationUn nombre d'annulations restantes au joueur un
     * @param peutAnnulerCoupDeux vrai si le joueur deux peut annuler un coup
     * @param compteurAnnulationDeux nombre d'annulations restantes au joueur
     * deux
     */
    public void mettreAJourAnnulation(
            boolean peutAnnulerCoupUn, boolean peutAnnulerCoupDeux,
            int compteurAnnulationUn, int compteurAnnulationDeux) {
        // Mise a jour de la vue d'annulation du joueur un
        this.vueJoueurUn.mettreAJourAnnulation(
                peutAnnulerCoupUn, compteurAnnulationUn);

        // Mise a jour de la vue d'annulation du joueur deux
        this.vueJoueurDeux.mettreAJourAnnulation(
                peutAnnulerCoupDeux, compteurAnnulationDeux);
    }

    /**
     * Met à jours les déplacement des joueurs et les affiche.
     *
     * @param deplacementsUn déplacements du joueur un
     * @param deplacementsDeux déplacements du joueur deux
     */
    public void mettreAJourDeplacements(
            Deplacements deplacementsUn,
            Deplacements deplacementsDeux) {
        this.vueJoueurUn.mettreAJourDeplacements(deplacementsUn);
        this.vueJoueurDeux.mettreAJourDeplacements(deplacementsDeux);
    }
}
