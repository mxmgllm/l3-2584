/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue.jeu;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import modele.deplacement.Deplacements;
import vue.jeu.menu.VueBarreDeMenu;

/**
 * Vue d'une partie, avec la barre de menu, la vue de la partie en cours, et une
 * vue qui s'affiche par dessus cella de la partie en cours une fois la pertie
 * terminée
 *
 * @author darven
 */
public class VuePartie {

    private final VueJeu vueJeu;
    private final VueBarreDeMenu vueMenu;
    private VuePartieTerminee vuePartieTerminee;
    private String etat;
    private final BorderPane racine;

    /**
     * Crée une nouvelle vue pour la partie
     *
     * @param typeJoueurUn type de joueur du joueur un
     * @param typeJoueurDeux type de joueur du joueur deux
     * @param gestionnaireAnnulationUn gestionnaire d'évènements pour
     * l'annulation de coup du joueur un
     * @param gestionnaireAnnulationDeux gestionnaire d'évènements pour
     * l'annulation de coup du joueur deux
     * @param gestionNouvellePartie gestionnaire d'évènements pour le lancement
     * de partie
     * @param gestionChargerPartie gestionnaire d'évènements pour le chargement
     * de partie
     * @param gestionnaireSauvegarde gestionnaire d'évènements pour la
     * sauvegarde de partie
     * @param gestionPartiesPrecedentes gestionnaire d'évènements pour afficher
     * les parties précédentes
     * @param gestionQuitter gestionnaire d'évènements pour quitter le jeu
     */
    public VuePartie(
            String typeJoueurUn,
            String typeJoueurDeux,
            EventHandler<ActionEvent> gestionnaireAnnulationUn,
            EventHandler<ActionEvent> gestionnaireAnnulationDeux,
            EventHandler<ActionEvent> gestionNouvellePartie,
            EventHandler<ActionEvent> gestionChargerPartie,
            EventHandler<ActionEvent> gestionnaireSauvegarde,
            EventHandler<ActionEvent> gestionPartiesPrecedentes,
            EventHandler<ActionEvent> gestionQuitter) {
        // Création de la vue du jeu
        vueJeu = new VueJeu(
                typeJoueurUn,
                typeJoueurDeux,
                gestionnaireAnnulationUn,
                gestionnaireAnnulationDeux);

        // Création de la vue du menu
        vueMenu = new VueBarreDeMenu(
                typeJoueurUn,
                typeJoueurDeux,
                gestionNouvellePartie,
                gestionChargerPartie,
                gestionnaireSauvegarde,
                gestionPartiesPrecedentes,
                gestionQuitter);

        etat = new String();
        racine = new BorderPane();
    }

    /**
     * Met à jour la vue
     *
     * @param scoreUn nouveau score du joueur un
     * @param scoreDeux nouveau score du joueur deux
     * @param nombreCoupsUn nouveau nombre de coup du joueur un
     * @param nombreCoupsDeux nouveau nombre de coup du joueur deux
     * @param grilleUn nouvelle grille du joueur un
     * @param grilleDeux nouvelle grille du joueur deux
     */
    public void mettreAJourVueJeu(Integer score1, Integer score2,
            Integer nombreCoups1, Integer nombreCoups2,
            Integer[][] grille1, Integer[][] grille2) {
        vueJeu.miseAJour(score1, score2, nombreCoups1, nombreCoups2,
                grille1, grille2);
    }

    /**
     * @param etat le nouvel état
     */
    public void setEtat(String etat) {
        this.etat = etat;
    }

    /**
     * Initialise la vue en GUI
     */
    private void initaliserVue() {
        // Creation et ajout de la vue du jeu
        this.vueJeu.initaliserVue();
        this.racine.setCenter(vueJeu);

        // Creation et ajout de la barre de menu
        this.racine.setTop(vueMenu);
        this.racine.autosize();

        /* Lien de la taille de la vue du jeu avec la taille de la vue sans la
         * vue du menu
         */
        this.vueJeu.prefHeightProperty().bind(
                this.racine.heightProperty().add(
                        this.vueMenu.heightProperty().negate()));
        this.vueJeu.prefWidthProperty().bind(
                this.racine.widthProperty());

    }

    /**
     * Initialise la vue et cree une scene avec
     *
     * @return une scene contenant les elements de la vue de la partie
     */
    public Scene initialiserScene() {
        initaliserVue();

        // ajout du contenu a la scene
        Scene scene = new Scene(racine, Color.WHITE);

        return scene;
    }

    /**
     * Met a jour la vue d'annulation de coup des deux joueurs
     *
     * @param peutAnnulerCoupUn vrai si le joueur 1 peut annuler un coup
     * @param compteurAnnulationUn nombre d'annulations restantes au joueur 1
     * @param peutAnnulerCoupDeux vrai si le joueur 2 peut annuler un coup
     * @param compteurAnnulationDeux nombre d'annulations restantes au joueur 2
     */
    public void mettreAJourAnnulation(
            boolean peutAnnulerCoupUn, boolean peutAnnulerCoupDeux,
            int compteurAnnulationUn, int compteurAnnulationDeux) {
        this.vueJeu.mettreAJourAnnulation(
                peutAnnulerCoupUn, peutAnnulerCoupDeux,
                compteurAnnulationUn, compteurAnnulationDeux);
    }

    /**
     * Affiche l'écran de fin de partie
     *
     * @param gagnant
     * @param theatre
     */
    public void afficherFinPartie(String gagnant, Stage theatre) {
        this.vuePartieTerminee = new VuePartieTerminee(this.vueJeu);
        this.vuePartieTerminee.setEtat(gagnant);

        /* Lien de la taille de la vue du jeu avec la taille de la vue sans la
         * vue du menu
         */
        this.vuePartieTerminee.prefHeightProperty().bind(
                this.racine.heightProperty().add(
                        this.vueMenu.heightProperty().negate()));
        this.vuePartieTerminee.prefWidthProperty().bind(
                this.racine.widthProperty());

        this.racine.setCenter(this.vuePartieTerminee);
        this.racine.autosize();
    }

    /**
     * Met à jours les déplacement des joueurs et les affiche.
     *
     * @param deplacementsUn déplacements du joueur un
     * @param deplacementsDeux déplacements du joueur deux
     */
    public void mettreAJourDeplacements(
            Deplacements deplacementsUn,
            Deplacements deplacementsDeux) {
        this.vueJeu.mettreAJourDeplacements(deplacementsUn, deplacementsDeux);
    }
}
