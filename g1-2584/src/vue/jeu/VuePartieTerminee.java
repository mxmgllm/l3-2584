/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue.jeu;

import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Rectangle;
import vue.ParametresGUI;
import static vue.ParametresGUI.TAILLE_ARRONDI_CASE;

/**
 * Vue qui s'affiche au dessus de la partie une fois celle-ci finie, assombrit
 * l'écran et affiche un message désignant le vainqueur
 *
 * @author darven
 */
public class VuePartieTerminee extends StackPane implements ParametresGUI {

    private String etat;
    private Label texte;
    private Rectangle arierePlan;
    private Rectangle arierePlanTexte;

    /**
     * Créé une nouvelle vue de fin de partie par dessus une vue de la partie en
     * cours
     *
     * @param vueJeu vue de la partie en cours
     */
    public VuePartieTerminee(VueJeu vueJeu) {
        this.getChildren().add(vueJeu);

        this.prefWidthProperty().bind(vueJeu.prefWidthProperty());
        this.prefHeightProperty().bind(vueJeu.prefHeightProperty());
        this.initialisationArrierePlan(vueJeu);
        this.initialisationTexte();
    }

    /**
     * Change l'état et met à jours le texte affiché
     *
     * @param etat le nouvel état
     */
    public void setEtat(String etat) {
        this.etat = etat;
        switch (this.etat) {
            case JOUEUR_DEUX:
                this.texte.setText(TEXTE_VICTOIRE_DEUX);
                break;

            case JOUEUR_UN:
                this.texte.setText(TEXTE_VICTOIRE_UN);
                break;

            case EGALITE:
                this.texte.setText(TEXTE_EGALITE);
                break;

            default:
                this.texte.setText(TEXTE_PARTIE_NON_TERMINEE);
        }
    }

    /**
     * Initialise l'arrière-plan tranparent aux dimentions de la vue de la
     * partie en cours
     *
     * @param vueJeu vue de la partie en cours
     */
    private void initialisationArrierePlan(VueJeu vueJeu) {
        // Creation de l'ariere-plan
        this.arierePlan = new Rectangle();
        this.arierePlan.setFill(COULEUR_FOND_FIN);

        // Lien de l'ariere-plan au conteneur
        this.arierePlan.widthProperty().bind(vueJeu.widthProperty());
        this.arierePlan.heightProperty().bind(vueJeu.heightProperty());
        this.getChildren().add(this.arierePlan);
    }

    /**
     * Initialise la zone de texte
     */
    private void initialisationTexte() {
        // Creation de l'ariere-plan
        this.arierePlanTexte = new Rectangle();
        this.arierePlanTexte.setFill(COULEUR_FOND_TEXTE_FIN);

        // Initialisation des angles de l'ariere-plan
        this.arierePlanTexte.arcWidthProperty().set(TAILLE_ARRONDI_CASE);
        this.arierePlanTexte.arcHeightProperty().bind(
                this.arierePlanTexte.arcWidthProperty());

        // Creation texte
        this.texte = new Label();
        this.texte.setTextFill(COULEUR_TEXTE_FIN);
        this.setEtat(ETAT_NON_TERMINE);

        // Lien de l'ariere-plan et du texte au conteneur
        this.arierePlanTexte.widthProperty().bind(
                this.texte.widthProperty().add(15));
        this.arierePlanTexte.heightProperty().bind(
                this.texte.heightProperty().add(15));

        // Conteneur supplementaire pour reparer le positionnement automatique
        BorderPane arierePlanTexteConteneur
                = new BorderPane(this.arierePlanTexte);

        this.getChildren().add(arierePlanTexteConteneur);
        this.getChildren().add(this.texte);
    }
}
