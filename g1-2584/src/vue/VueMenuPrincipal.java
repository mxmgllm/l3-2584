/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

/**
 * Vue du menu principal, contenant les boutons pour commencer une partie,
 * charger une partie sauvegardée, voir les parties précédentes et quitter le
 * jeu
 *
 * @author emarquer
 */
public class VueMenuPrincipal implements ParametresGUI {

    private Text texteMenuPrincipal;
    private Button boutonNouvellePartie;
    private Button boutonChargerPartie;
    private Button boutonPartiesPrecedentes;
    private Button boutonQuitter;
    private VBox centreDeLaVue;
    private BorderPane racine;

    /**
     * Initialise la vue en GUI
     *
     * @param gestionNouvellePartie gestionnaire d'évènements pour le lancement
     * de partie
     * @param gestionChargerPartie gestionnaire d'évènements pour le chargement
     * de partie
     * @param gestionPartiesPrecedentes gestionnaire d'évènements pour afficher
     * les parties précédentes
     * @param gestionQuitter gestionnaire d'évènements pour quitter le jeu
     */
    private void initaliserVue(
            EventHandler<ActionEvent> gestionNouvellePartie,
            EventHandler<ActionEvent> gestionChargerPartie,
            EventHandler<ActionEvent> gestionPartiesPrecedentes,
            EventHandler<ActionEvent> gestionQuitter) {

        // Creation du texte de titre
        this.texteMenuPrincipal = new Text(TEXTE_TITRE_MENU_PRINCIPAL);
        this.texteMenuPrincipal.setTextAlignment(TextAlignment.CENTER);

        // Creation des boutons
        this.initialiserBoutons(
                gestionNouvellePartie,
                gestionChargerPartie,
                gestionPartiesPrecedentes,
                gestionQuitter);

        // Creation du centre de la vue
        this.centreDeLaVue = new VBox();
        this.centreDeLaVue.setPadding(new Insets(25, 25, 25, 25));
        this.centreDeLaVue.setSpacing(10);
        this.centreDeLaVue.setAlignment(Pos.CENTER);
        this.centreDeLaVue.setFillWidth(true);

        // Ajout du titre au conteneur de centre de la vue
        this.centreDeLaVue.getChildren().add(this.texteMenuPrincipal);

        // Ajout des boutons au conteneur de centre de la vue
        this.centreDeLaVue.getChildren().add(this.boutonNouvellePartie);
        this.centreDeLaVue.getChildren().add(this.boutonChargerPartie);
        this.centreDeLaVue.getChildren().add(this.boutonPartiesPrecedentes);
        this.centreDeLaVue.getChildren().add(this.boutonQuitter);

        // Ajout du conteneur de centre de la vue a la vue
        this.racine = new BorderPane(this.centreDeLaVue);
        // TODO this.racine.setTop(this.barreDeMenu);
        this.racine.autosize();
    }

    /**
     * Initialise la vue et cree une scene avec
     *
     * @param gestionNouvellePartie gestionnaire d'évènements pour le lancement
     * de partie
     * @param gestionChargerPartie gestionnaire d'évènements pour le chargement
     * de partie
     * @param gestionPartiesPrecedentes gestionnaire d'évènements pour afficher
     * les parties précédentes
     * @param gestionQuitter gestionnaire d'évènements pour quitter le jeu
     * @return une scene contenant les elements de la vue de la partie
     */
    public Scene initialiserScene(
            EventHandler<ActionEvent> gestionNouvellePartie,
            EventHandler<ActionEvent> gestionChargerPartie,
            EventHandler<ActionEvent> gestionPartiesPrecedentes,
            EventHandler<ActionEvent> gestionQuitter) {
        initaliserVue(
                gestionNouvellePartie,
                gestionChargerPartie,
                gestionPartiesPrecedentes,
                gestionQuitter);

        // ajout du contenu a la scene
        Scene scene = new Scene(this.racine, Color.WHITE);

        return scene;
    }

    /**
     * Initialise les boutons
     *
     * @param gestionNouvellePartie gestionnaire d'évènements pour le lancement
     * de partie
     * @param gestionChargerPartie gestionnaire d'évènements pour le chargement
     * de partie
     * @param gestionPartiesPrecedentes gestionnaire d'évènements pour afficher
     * les parties précédentes
     * @param gestionQuitter gestionnaire d'évènements pour quitter le jeu
     */
    private void initialiserBoutons(
            EventHandler<ActionEvent> gestionNouvellePartie,
            EventHandler<ActionEvent> gestionChargerPartie,
            EventHandler<ActionEvent> gestionPartiesPrecedentes,
            EventHandler<ActionEvent> gestionQuitter) {
        // Creation des boutons
        this.boutonNouvellePartie = new Button(TEXTE_BOUTON_NOUVELLE_PARTIE);
        this.boutonChargerPartie = new Button(TEXTE_BOUTON_CHARGER);
        this.boutonPartiesPrecedentes
                = new Button(TEXTE_BOUTON_PARTIES_PRECEDENTES);
        this.boutonQuitter = new Button(TEXTE_BOUTON_QUITTER);

        // Lien de la largeur des boutons entre eux
        this.boutonNouvellePartie.prefWidthProperty().bind(
                this.boutonPartiesPrecedentes.widthProperty());
        this.boutonChargerPartie.prefWidthProperty().bind(
                this.boutonPartiesPrecedentes.widthProperty());
        this.boutonQuitter.prefWidthProperty().bind(
                this.boutonPartiesPrecedentes.widthProperty());

        // Activation des gestionnaires
        this.boutonNouvellePartie.setOnAction(gestionNouvellePartie);
        this.boutonChargerPartie.setOnAction(gestionChargerPartie);
        this.boutonPartiesPrecedentes.setOnAction(gestionPartiesPrecedentes);
        this.boutonQuitter.setOnAction(gestionQuitter);
    }
}
