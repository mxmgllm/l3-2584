/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;

import java.util.ArrayList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.layout.GridPane;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

/**
 * Vue du menu de choix du types des joueurs, avant de démarer une nouvelle
 * partie
 *
 * @author emarquer
 */
public class VueMenuTypeJoueur implements ParametresGUI {

    private Text texteNouvellePartie;
    private Text texteTypeJoueurUn;
    private Text texteTypeJoueurDeux;
    private ComboBox<String> typeJoueurUn;
    private ComboBox<String> typeJoueurDeux;
    private Button boutonLancerPartie;
    private Button boutonRetour;
    private GridPane menusDeroulants;
    private VBox centreDeLaVue;
    private BorderPane racine;

    /**
     * Initialise la vue en GUI
     *
     * @param gestionnaireLancerPartie gestionnaire d'évènements pour le
     * lancement de partie
     * @param gestionnaireBoutonRetour gestionnaire pour l'action du bouton
     * retour
     */
    private void initaliserVue(
            EventHandler<ActionEvent> gestionnaireLancerPartie,
            EventHandler<ActionEvent> gestionnaireBoutonRetour) {

        // Creation du texte de titre
        this.texteNouvellePartie = new Text(TEXTE_TITRE_NOUVELLE_PARTIE);
        this.texteNouvellePartie.setTextAlignment(TextAlignment.CENTER);

        // Creation des bouton
        this.initialiserBoutons(
                gestionnaireLancerPartie, gestionnaireBoutonRetour);

        // Initialisation des menus deroulants
        this.initialiserConteneurMenusDeroulants();

        // Creation du centre de la vue
        this.centreDeLaVue = new VBox();
        this.centreDeLaVue.setPadding(new Insets(25, 25, 25, 25));
        this.centreDeLaVue.setAlignment(Pos.CENTER);

        // Ajout du titre et des menus deroulants au conteneur de centre de la vue
        this.centreDeLaVue.getChildren().add(this.texteNouvellePartie);

        // Ajout de la zone des menus deroulants au conteneur de centre de la vue
        this.centreDeLaVue.getChildren().add(this.menusDeroulants);

        // Ajout de la zone des menus deroulants au conteneur de centre de la vue
        this.centreDeLaVue.getChildren().add(this.boutonLancerPartie);

        // Ajout du conteneur de centre de la vue a la vue
        this.racine = new BorderPane(this.centreDeLaVue);
        this.racine.setBottom(this.boutonRetour);
        this.boutonRetour.setAlignment(Pos.CENTER);
        // TODO this.racine.setTop(this.barreDeMenu);
        this.racine.autosize();
    }

    /**
     * Initialise les boutons
     *
     * @param gestionnaireLancerPartie gestionnaire d'évènements pour le
     * lancement de partie
     * @param gestionnaireBoutonRetour gestionnaire pour l'action du bouton
     * retour
     */
    private void initialiserBoutons(
            EventHandler<ActionEvent> gestionnaireLancerPartie,
            EventHandler<ActionEvent> gestionnaireBoutonRetour) {
        // Creation du bouton de lancement de partie
        this.boutonLancerPartie = new Button(TEXTE_BOUTON_LANCER_PARTIE);
        this.boutonLancerPartie.setOnAction(gestionnaireLancerPartie);

        // Creation du bouton de retour au menu
        this.boutonRetour = new Button(TEXTE_BOUTON_RETOUR);
        this.boutonRetour.setOnAction(gestionnaireBoutonRetour);
    }

    /**
     * Initialise le conteneur des menus deroulants des types des deux joueurs
     */
    private void initialiserConteneurMenusDeroulants() {
        // Initialisation des menus deroulants
        this.initialiserMenusDeroulants();

        // Creation du nom des menus deroulants
        this.texteTypeJoueurUn = new Text(String.format(TEXTE_NOM_UN, ""));
        this.texteTypeJoueurDeux = new Text(String.format(TEXTE_NOM_DEUX, ""));

        // Creation de la grille contenant les menus deroulans et leurs noms
        this.menusDeroulants = new GridPane();
        this.menusDeroulants.setAlignment(Pos.CENTER);
        this.menusDeroulants.setHgap(10);
        this.menusDeroulants.setVgap(10);
        this.menusDeroulants.setPadding(new Insets(25, 25, 25, 25));

        // Ajout des menus deroulants et de leur nom a la grille
        this.menusDeroulants.add(this.texteTypeJoueurUn, 0, 0);
        this.menusDeroulants.add(this.typeJoueurUn, 0, 1);
        this.menusDeroulants.add(this.texteTypeJoueurDeux, 1, 0);
        this.menusDeroulants.add(this.typeJoueurDeux, 1, 1);
    }

    /**
     * Initialise les menus deroulants des types des deux joueurs
     */
    private void initialiserMenusDeroulants() {
        // Creation des menus deroulants
        this.typeJoueurUn = new ComboBox();
        this.typeJoueurDeux = new ComboBox();

        // Creation des options
        ArrayList<String> types = new ArrayList<>();
        types.add(JOUEUR_HUMAIN);
        types.add(JOUEUR_ORDINATEUR);
        types.add(JOUEUR_IA);

        // Ajout des options aux menus deroulants
        this.typeJoueurUn.getItems().addAll(types);
        this.typeJoueurDeux.getItems().addAll(types);

        // Definition de la valeur par defaut
        this.typeJoueurUn.setValue(types.get(0));
        this.typeJoueurDeux.setValue(types.get(0));

        this.typeJoueurUn.setEditable(false);
        this.typeJoueurDeux.setEditable(false);
    }

    /**
     * Initialise la vue et crée une scene avec
     *
     * @param gestionnaireLancerPartie gestionnaire d'évènnements pour le
     * lancement de partie
     * @param gestionnaireBoutonRetour gestionnaire pour l'action du bouton
     * retour
     * @return une scene contenant les elements de la vue de la partie
     */
    public Scene initialiserScene(
            EventHandler<ActionEvent> gestionnaireLancerPartie,
            EventHandler<ActionEvent> gestionnaireBoutonRetour) {
        initaliserVue(gestionnaireLancerPartie, gestionnaireBoutonRetour);

        // ajout du contenu a la scene
        Scene scene = new Scene(this.racine, Color.WHITE);

        return scene;
    }

    /**
     * @return la valeur selectionnee pour le type du joueur un
     */
    public String retournerTypeJoueurUn() {
        return this.typeJoueurUn.getValue();
    }

    /**
     * @return la valeur selectionnee pour le type du joueur deux
     */
    public String retournerTypeJoueurDeux() {
        return this.typeJoueurDeux.getValue();
    }
}
