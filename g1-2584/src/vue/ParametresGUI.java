/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;

import javafx.scene.paint.Color;
import javafx.util.Duration;
import modele.Parametres;

/**
 * Contient l'ensemble des paramètres de la GUI.
 *
 * @author darven
 */
public interface ParametresGUI extends Parametres {

    /*
     * Texte des colonnes de vue des parties en BDD
     */
    public static final String TITRE_COLONNE_ID = "Identifiant";
    public static final String TITRE_COLONNE_DATE = "Date";
    public static final String TITRE_COLONNE_SCORE = "Score";
    public static final String TITRE_COLONNE_NB_COUPS = "Nombre de coups";
    public static final String TITRE_COLONNE_TUILE_MAX = "Tuile maximale";
    public static final String TITRE_COLONNE_JOUEUR_UN = "Joueur 1";
    public static final String TITRE_COLONNE_JOUEUR_DEUX = "Joueur 2";

    /*
     * Texte des boutons
     */
    static final String TEXTE_BOUTON_RETOUR = "Retour";
    static final String TEXTE_BOUTON_PARTIES_PRECEDENTES
            = "Parties précédentes";
    static final String TEXTE_BOUTON_SAUVEGARDE = "Sauvegarder";
    static final String TEXTE_BOUTON_CHARGER = "Charger";
    static final String TEXTE_BOUTON_ANNULER = "Annuler (%s)";
    static final String TEXTE_FLOTANT_BOUTON_ANNULER
            = "Cliquer pour annuler le dernier coup";
    static final String TEXTE_BOUTON_NOUVELLE_PARTIE = "Nouvelle partie";
    static final String TEXTE_BOUTON_LANCER_PARTIE = "Lancer la partie";
    static final String TEXTE_BOUTON_QUITTER = "Quitter";
    static final String TEXTE_BOUTON_AFFICHER_CONTROLES
            = "Afficher les controles";
    static final String TEXTE_BOUTON_AFFICHER_SUITE
            = "Afficher la suite de Fibonacci";

    /*
     * Texte des labels
     */
    static final String TEXTE_TITRE_NOUVELLE_PARTIE = "Nouvelle partie";
    static final String TEXTE_TITRE_MENU_PRINCIPAL = "2584";

    /*
     * Texte de fin de partie
     */
    static final String TEXTE_VICTOIRE_UN = "Victoire du joueur 1 !";
    static final String TEXTE_VICTOIRE_DEUX = "Victoire du joueur 2 !";
    static final String TEXTE_EGALITE = "Egalité !";
    static final String TEXTE_PARTIE_NON_TERMINEE = "Partie non terminée.";

    /*
     * Texte de nom de joueur
     */
    static final String TEXTE_NOM_UN = "Joueur 1 : %s";
    static final String TEXTE_NOM_DEUX = "Joueur 2 : %s";
    static final String TEXTE_NOM_JOUEUR_HUMAIN = "Humain",
            TEXTE_NOM_JOUEUR_ORDINATEUR = "Ordinateur",
            TEXTE_NOM_JOUEUR_IA = "IA";

    /*
     * Texte des touches des joueurs
     */
    static final String TEXTE_TOUCHES_UN = "Touches : "
            + "«" + TOUCHE_GAUCHE_UN + "» (gauche), "
            + "«" + TOUCHE_BAS_UN + "» (bas), "
            + "«" + TOUCHE_DROITE_UN + "» (droite), "
            + "«" + TOUCHE_HAUT_UN + "» (haut)";
    static final String TEXTE_TOUCHES_DEUX = "Touches : "
            + "«" + TOUCHE_GAUCHE_DEUX + "» (gauche), "
            + "«" + TOUCHE_BAS_DEUX + "» (bas), "
            + "«" + TOUCHE_DROITE_DEUX + "» (droite), "
            + "«" + TOUCHE_HAUT_DEUX + "» (haut)";
    static final String TOUCHES_JOUEURS
            = String.format(TEXTE_NOM_UN, TEXTE_TOUCHES_UN)
            + String.format(TEXTE_NOM_DEUX, TEXTE_TOUCHES_DEUX);
    static final String TEXTE_TOUCHES_ORDINATEUR = "Controlé par l'ordinatieur";
    static final String TEXTE_TOUCHES_IA = "Controlé par l'IA";

    /*
     * Texte du score
     */
    static final String TEXTE_SCORE = "Score : %s";

    /*
     * Texte de nombre de coups
     */
    static final String TEXTE_NOMBRE_COUPS = "Nombre de coups : %d";

    /*
     * Parametres des cases (couleur, arriere-plan, taille)
     */
    static final double TAILLE_CASE = 55;
    static final double TAILLE_ARRONDI_CASE = 15;
    static final Color COULEUR_TEXTE_CASES = Color.WHITE;
    static final int TAILLE_TEXTE_CASES = 20;
    static final Color COULEUR_CASE_VIDE = Color.LIGHTGRAY;
    static final Color[] COULEURS_CASES = new Color[]{
        Color.rgb(229, 0, 179), // rang 0
        Color.rgb(226, 6, 167), // rang 1
        Color.rgb(224, 13, 156), // rang 2
        Color.rgb(221, 20, 145), // rang 3
        Color.rgb(219, 27, 134), // rang 4
        Color.rgb(217, 34, 123), // rang 5
        Color.rgb(214, 40, 111), // rang 6
        Color.rgb(212, 47, 100), // rang 7
        Color.rgb(210, 54, 89), // rang 9
        Color.rgb(207, 61, 78), // rang 10
        Color.rgb(205, 68, 66), // rang 11
        Color.rgb(202, 74, 55), // rang 12
        Color.rgb(200, 81, 44), // rang 13
        Color.rgb(198, 88, 33), // rang 14
        Color.rgb(195, 95, 22), // rang 15
        Color.rgb(193, 102, 11), // rang 16
        Color.rgb(191, 109, 0) // rang 17
    };

    /*
     * Parametres de l'affichage de fin de partie
     */
    static final Color COULEUR_TEXTE_FIN = Color.BLACK;
    static final Color COULEUR_FOND_TEXTE_FIN = Color.LIGHTGRAY;
    static final Color COULEUR_FOND_FIN = Color.BLACK.deriveColor(0, 1, 1, 0.5);

    /*
     * Paramètres d'animation
     */
    static final Duration TEMPS_APPARITION = Duration.seconds(0.5);
    static final Duration TEMPS_TRANSLATION_UNE_CASE = Duration.seconds(0.25);
}
