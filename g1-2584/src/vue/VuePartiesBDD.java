/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import modele.bdd.PartieBDD;

/**
 * Table de vues de parties en BDD
 *
 * @author emarquer
 */
public class VuePartiesBDD extends TableView<VuePartieBDD>
        implements ParametresGUI {

    private ObservableList<VuePartieBDD> vuesPartiesBDD;
    private TableColumn<VuePartieBDD, Integer> colonneId;
    private TableColumn<VuePartieBDD, Integer> colonneScoreUn;
    private TableColumn<VuePartieBDD, Integer> colonneScoreDeux;
    private TableColumn<VuePartieBDD, Integer> colonneNbCoupUn;
    private TableColumn<VuePartieBDD, Integer> colonneNbCoupDeux;
    private TableColumn<VuePartieBDD, String> colonneTuileMaxUn;
    private TableColumn<VuePartieBDD, String> colonneTuileMaxDeux;
    private TableColumn<VuePartieBDD, String> colonneJoueurUn;
    private TableColumn<VuePartieBDD, String> colonneJoueurDeux;
    private TableColumn<VuePartieBDD, String> colonneDate;

    /**
     * Créé une vue des parties en BDD
     *
     * @param partiesBDD parties à afficher
     */
    public VuePartiesBDD(ArrayList<PartieBDD> partiesBDD) {
        initaliserVueParties(partiesBDD);
    }

    /**
     * Initialise la vue en GUI des parties
     *
     * @param partiesBDD parties à afficher
     */
    private void initaliserVueParties(ArrayList<PartieBDD> partiesBDD) {
        vuesPartiesBDD = FXCollections.observableArrayList();

        // Initialisation des colonnes
        initialiserColonnes();

        // Ajoute toutes les parties en tant que lignes
        for (PartieBDD partieBDD : partiesBDD) {
            vuesPartiesBDD.add(new VuePartieBDD(partieBDD));
        }
        this.setItems(vuesPartiesBDD);
    }

    /**
     * Ajoute le titre des colonnes, ainsi que les valeurs des
     * {@code VuePartieBDD} qu'elles doivent afficher
     */
    private void initialiserColonnes() {
        // Définition de l'en-tête des colonnes
        colonneId = new TableColumn<>(TITRE_COLONNE_ID);
        colonneDate = new TableColumn<>(TITRE_COLONNE_DATE);
        colonneScoreUn = new TableColumn<>(TITRE_COLONNE_SCORE);
        colonneScoreDeux = new TableColumn<>(TITRE_COLONNE_SCORE);
        colonneNbCoupUn = new TableColumn<>(TITRE_COLONNE_NB_COUPS);
        colonneNbCoupDeux = new TableColumn<>(TITRE_COLONNE_NB_COUPS);
        colonneTuileMaxUn = new TableColumn<>(TITRE_COLONNE_TUILE_MAX);
        colonneTuileMaxDeux = new TableColumn<>(TITRE_COLONNE_TUILE_MAX);
        colonneJoueurUn = new TableColumn<>(TITRE_COLONNE_JOUEUR_UN);
        colonneJoueurDeux = new TableColumn<>(TITRE_COLONNE_JOUEUR_DEUX);

        // Définition des valeurs à récupérer pour chaque colonne
        colonneId.setCellValueFactory(cellule
                -> cellule.getValue().getId().asObject());
        colonneDate.setCellValueFactory(cellule
                -> cellule.getValue().getDate());
        colonneScoreUn.setCellValueFactory(cellule
                -> cellule.getValue().getScoreUn().asObject());
        colonneScoreDeux.setCellValueFactory(cellule
                -> cellule.getValue().getScoreDeux().asObject());
        colonneNbCoupUn.setCellValueFactory(cellule
                -> cellule.getValue().getNbCoupUn().asObject());
        colonneNbCoupDeux.setCellValueFactory(cellule
                -> cellule.getValue().getNbCoupDeux().asObject());
        colonneTuileMaxUn.setCellValueFactory(cellule
                -> cellule.getValue().getTuileMaxUn());
        colonneTuileMaxDeux.setCellValueFactory(cellule
                -> cellule.getValue().getTuileMaxDeux());

        // Ajout des colonnes aux colonnes de joueurs
        colonneJoueurUn.getColumns().addAll(
                colonneScoreUn,
                colonneNbCoupUn,
                colonneTuileMaxUn);
        colonneJoueurDeux.getColumns().addAll(
                colonneScoreDeux,
                colonneNbCoupDeux,
                colonneTuileMaxDeux);

        // Ajout des colonnes à la table
        this.getColumns().addAll(colonneId, colonneDate,
                colonneJoueurUn, colonneJoueurDeux);
    }
}
