/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;

import java.util.ArrayList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import modele.bdd.PartieBDD;

/**
 * Vue des parties en BDD, avec un bouton de retour en arrière
 *
 * @author emarquer
 */
public class VueBDD implements ParametresGUI {

    private BorderPane racine;
    private VuePartiesBDD vueParties;
    private Button boutonRetour;

    /**
     * Initialise la vue et crée une scène avec.
     *
     * @param partiesBDD parties à afficher
     * @param gestionnaireBoutonRetour gestionnaire pour l'action du bouton
     * retour
     * @return une scène contenant les éléments de la vue de la partie
     */
    public Scene initialiserScene(ArrayList<PartieBDD> partiesBDD,
            EventHandler<ActionEvent> gestionnaireBoutonRetour) {
        initaliserVue(partiesBDD, gestionnaireBoutonRetour);

        // ajout du contenu a la scene
        Scene scene = new Scene(racine, Color.WHITE);

        return scene;
    }

    /**
     * Initialise la vue en GUI
     *
     * @param partiesBDD parties à afficher
     * @param gestionnaireBoutonRetour gestionnaire pour l'action du bouton
     * retour
     */
    private void initaliserVue(ArrayList<PartieBDD> partiesBDD,
            EventHandler<ActionEvent> gestionnaireBoutonRetour) {
        this.vueParties = new VuePartiesBDD(partiesBDD);
        this.boutonRetour = new Button(TEXTE_BOUTON_RETOUR);
        this.boutonRetour.setOnAction(gestionnaireBoutonRetour);

        this.racine = new BorderPane(vueParties);
        this.racine.setBottom(boutonRetour);
    }
}
