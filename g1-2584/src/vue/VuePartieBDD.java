/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;

import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleStringProperty;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import modele.bdd.PartieBDD;
import vue.jeu.VueCase;

/**
 * Vue d'une partie en BDD
 *
 * @author emarquer
 */
public class VuePartieBDD {

    private final IntegerProperty id;
    private final IntegerProperty scoreUn;
    private final IntegerProperty scoreDeux;
    private final IntegerProperty nbCoupUn;
    private final IntegerProperty nbCoupDeux;
    private final StringProperty tuileMaxUn;
    private final StringProperty tuileMaxDeux;
    private final StringProperty date;

    /**
     * Crée une vue de partie en BDD adaptée à l'affichage en
     * {@code VuePartiesBDD} à partir d'une {@code PartieBDD}
     *
     * @param partieBDD partie en BDD à transformer
     */
    public VuePartieBDD(PartieBDD partieBDD) {
        this.id = new SimpleIntegerProperty(partieBDD.getId());
        this.scoreUn = new SimpleIntegerProperty(partieBDD.getScoreUn());
        this.scoreDeux = new SimpleIntegerProperty(partieBDD.getScoreDeux());
        this.nbCoupUn = new SimpleIntegerProperty(partieBDD.getNbCoupUn());
        this.nbCoupDeux = new SimpleIntegerProperty(partieBDD.getNbCoupDeux());
        this.tuileMaxUn = new SimpleStringProperty(
                VueCase.texteRang(partieBDD.getTuileMaxUn()));
        this.tuileMaxDeux = new SimpleStringProperty(
                VueCase.texteRang(partieBDD.getTuileMaxDeux()));
        this.date = new SimpleStringProperty(partieBDD.getDate());
    }

    /**
     * @return the id
     */
    public IntegerProperty getId() {
        return id;
    }

    /**
     * @return the scoreUn
     */
    public IntegerProperty getScoreUn() {
        return scoreUn;
    }

    /**
     * @return the scoreDeux
     */
    public IntegerProperty getScoreDeux() {
        return scoreDeux;
    }

    /**
     * @return the nbCoupUn
     */
    public IntegerProperty getNbCoupUn() {
        return nbCoupUn;
    }

    /**
     * @return the nbCoupDeux
     */
    public IntegerProperty getNbCoupDeux() {
        return nbCoupDeux;
    }

    /**
     * @return the tuileMaxUn
     */
    public StringProperty getTuileMaxUn() {
        return tuileMaxUn;
    }

    /**
     * @return the tuileMaxDeux
     */
    public StringProperty getTuileMaxDeux() {
        return tuileMaxDeux;
    }

    /**
     * @return the date
     */
    public StringProperty getDate() {
        return date;
    }

}
