/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

/**
 * Contient les paramètres du jeu
 *
 * @author mg
 */
public interface Parametres {

    static final int TAILLE = 4,
            CELLULE_VIDE = -1;

    static final boolean VERTICAL = true,
            SENS_DE_LECTURE = true;

    static final String ETAT_NON_TERMINE = "En cours",
            ETAT_PERDU = "Perd",
            ETAT_GAGNE = "Gagne",
            EGALITE = "Egalité",
            JOUEUR = "Joueur",
            JOUEUR_UN = JOUEUR + " 1",
            JOUEUR_DEUX = JOUEUR + " 2",
            PARTIE_COMMENCE = "La partie commence",
            PARTIE_TERMINEE = "La partie est terminée";

    static final String JOUEUR_HUMAIN = "Humain",
            JOUEUR_ORDINATEUR = "Ordinateur (coups aléatoires)",
            JOUEUR_IA = "Intelligence Artificielle";

    static final int[] SUITE = new int[]{1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584};
    static final int OBJECTIF = SUITE.length - 1,
            NOMBRE_ANNULATION = 5;

    static final String TOUCHE_HAUT_UN = "z";
    static final String TOUCHE_BAS_UN = "s";
    static final String TOUCHE_GAUCHE_UN = "q";
    static final String TOUCHE_DROITE_UN = "d";
    static final String TOUCHE_HAUT_DEUX = "o";
    static final String TOUCHE_BAS_DEUX = "l";
    static final String TOUCHE_GAUCHE_DEUX = "k";
    static final String TOUCHE_DROITE_DEUX = "m";

    static final int[] RANG_POSSIBLE_APRES_MOUVEMENT = {0, 1};
    static final int[] PROBABILITE_RANG_APRES_MOUVEMENT
            = {RANG_POSSIBLE_APRES_MOUVEMENT[0],
                RANG_POSSIBLE_APRES_MOUVEMENT[0],
                RANG_POSSIBLE_APRES_MOUVEMENT[0],
                RANG_POSSIBLE_APRES_MOUVEMENT[1]};

}
