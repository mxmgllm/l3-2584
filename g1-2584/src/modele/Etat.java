/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

/**
 * Représente un état de la partie d'un joueur
 *
 * @author mg
 */
public class Etat {

    private final boolean TERMINEE,
            VICTOIRE;

    /**
     * Construction d'un état
     *
     * @param TERMINEE
     * @param VICTOIRE
     */
    public Etat(boolean TERMINEE, boolean VICTOIRE) {
        this.TERMINEE = TERMINEE;
        this.VICTOIRE = VICTOIRE;
    }

    /**
     * on définit l'égalité comme une comparaison entre la valeur terminee de
     * l'objet et la valeur de victoire
     *
     * @param etat
     * @return vraie si les états sont égaux
     */
    @Override
    public boolean equals(Object etat) {
        if (etat instanceof Etat) {
            return this.TERMINEE == ((Etat) etat).isTerminee() && this.VICTOIRE == ((Etat) etat).isVictoire();
        }
        return false;
    }

//------------------------------------------------------------------------------
    /**
     * @return vrai si le joueur a perdu
     */
    public boolean retournerPerdu() {
        return TERMINEE && !VICTOIRE;
    }

    /**
     * @return vraie si le jeu est terminee
     */
    public boolean isTerminee() {
        return TERMINEE;
    }

    /**
     * @return vraie si le joueur à gagner
     */
    public boolean isVictoire() {
        return VICTOIRE;
    }

}
