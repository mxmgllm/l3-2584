/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import java.io.Serializable;

/**
 * Représente une partie du jeu 2584
 *
 * @author mg
 */
public class Partie implements Parametres, Serializable {

    private Jeu jeu;

    /**
     * Construction d'une partie
     *
     * @param jeu jeu crée
     */
    public Partie(Jeu jeu) {
        this.jeu = jeu;
    }

    /**
     *
     * @return vrai si la partie est terminée
     */
    public boolean isTerminee() {
        String retour = jeu.retournerEtatJeu().retournerGagnant();
        return retour.equals(JOUEUR_UN) || retour.equals(JOUEUR_DEUX)
                || retour.equals(EGALITE);

    }

    /**
     * @return le jeu
     */
    public Jeu getJeu() {
        return jeu;
    }

}
