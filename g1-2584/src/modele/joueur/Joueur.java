/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.joueur;

import java.io.Serializable;
import modele.Etat;
import modele.Grille;
import modele.Parametres;
import modele.deplacement.Deplacements;

/**
 * Représente un joueur abstrait
 *
 * @author mg
 */
public abstract class Joueur implements Serializable {

    protected Grille grille;
    protected Integer nbCoups;

    /**
     * Construction d'un joueur à partir d'une grille
     *
     * @param grille
     */
    public Joueur(Grille grille) {
        this.grille = grille;
        nbCoups = 0;
    }

    /**
     * @return la grille du joueur
     */
    public Grille getGrille() {
        return grille;
    }

    /**
     * @return le nombre de coups effectués par le joueur.
     */
    public Integer getNbCoups() {
        return nbCoups;
    }

    /**
     * @return les derniers déplacements de cases
     */
    public Deplacements retournerDeplacements() {
        return grille.getDerniersDeplacements();
    }

    /**
     * Effectue un mouvement
     *
     * @param mouvement
     * @return vrai si le mouvement à été effectué
     */
    public boolean jouerUnCoup(String mouvement) {
        boolean effectue = false;
        switch (mouvement) {
            case "GAUCHE":
                effectue = this.grille.effectuerMouvement(false, false);
                break;
            case "HAUT":
                effectue = this.grille.effectuerMouvement(false, true);
                break;
            case "DROITE":
                effectue = this.grille.effectuerMouvement(true, false);
                break;
            case "BAS":
                effectue = this.grille.effectuerMouvement(true, true);
                break;
        }
        if (effectue) {
            nbCoups++;
        }
        return effectue;

    }

    /**
     *
     * @return le score du joueur
     */
    public int retournerScore() {
        return grille.getScore();
    }

    /**
     *
     * @return l'état de la partie du joueur à partir de sa grille.
     */
    public Etat retournerObjectifAtteint() {
        return this.grille.evaluerEtat();
    }

    /**
     *
     * @return le tableau 2 dimensions qui contient les rangs.
     */
    public Integer[][] retournerCases() {
        return grille.retournerCases();
    }

    /**
     *
     * @param joueur
     * @return une chaine qui représente le type concret du joueur
     */
    public static String retournerClasseJoueur(Joueur joueur) {
        if (joueur instanceof IntelligenceArtificielle) {
            return Parametres.JOUEUR_IA;
        } else if (joueur instanceof Ordinateur) {
            return Parametres.JOUEUR_ORDINATEUR;
        } else {
            return Parametres.JOUEUR_HUMAIN;
        }
    }
}
