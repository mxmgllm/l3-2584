/*
 *
 */
package modele.joueur;

import java.io.Serializable;
import modele.Grille;
import modele.Parametres;
import modele.deplacement.Deplacements;

/**
 * Représente un joueur humain du 2584
 *
 * @author mg
 */
public class Humain extends Joueur implements Parametres, Serializable {

    private Grille tourPrecedent;
    private int compteurAnnulation;

    /**
     * Construction d'un humain
     *
     * @param grille
     */
    public Humain(Grille grille) {
        super(grille);
        tourPrecedent = null;
        compteurAnnulation = NOMBRE_ANNULATION;
    }

    /**
     * Applique le mouvement en fonction de la chaine
     *
     * @param mouvement
     * @return vrai si le mouvement à été effectué
     */
    @Override
    public boolean jouerUnCoup(String mouvement) {
        //copie
        Grille copie = (Grille) this.grille.clone();

        boolean effectue = super.jouerUnCoup(mouvement);

        //cas ou le mouvement est validé
        if (effectue) {
            this.tourPrecedent = copie;

        }
        return effectue;
    }

//------------------------------------------------------------------------------   
    /**
     * Annule le coup précedent si le joueur n'a pas dépassé le quota
     *
     * @return vrai l'annulation a été effectué
     */
    public boolean annulerCoup() {

        //cas annulable
        if (peutAnnulerUnCoup()) {
            decrementationCompteurAnnulation();

            this.grille = (Grille) tourPrecedent.clone();
            Deplacements.changerAncien(this.grille.getCases());
            tourPrecedent = null;

            return true;

        }
        return false;

    }

    /**
     * incrémente de 1 le compteur d'annulation
     */
    private void decrementationCompteurAnnulation() {
        compteurAnnulation--;
    }

    /**
     * Prend en compte l'existance d'un tour précédent et le nombre
     * d'annulations restantes pour déterminer la possibiliter d'annuler
     *
     * @return vrai si le joueur peut annuler un coup
     */
    public boolean peutAnnulerUnCoup() {
        return compteurAnnulation > 0 && tourPrecedent != null;
    }

    /**
     * @return le nombre d'annulations restantes
     */
    public int getCompteurAnnulation() {
        return compteurAnnulation;
    }
}
