/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.joueur;

import java.io.Serializable;
import java.util.Objects;
import modele.Grille;
import modele.operation.OperationMatricielle;

/**
 * Représente l'intelligence artificielle, son objectif est de perdre le plus
 * rapidement possible.
 *
 * @author Alex
 */
public class IntelligenceArtificielle extends Ordinateur
        implements Serializable {

    /**
     * Construction d'un joueur IA
     *
     * @param grille
     */
    public IntelligenceArtificielle(Grille grille) {
        super(grille);
    }

    @Override
    /**
     * Joue le coup qui amène à la perte.
     */
    public void jouerUnCoup() {
        float evaluation = 0;
        String choix = new String();

        //parcours des directions
        for (String dir : DIRECTION_ORDI) {
            if (evaluationCoup(dir) > evaluation) {
                evaluation = evaluationCoup(dir);

                choix = dir;
            }
        }

        jouerUnCoup(choix);
    }
//------------------------------------------------------------------------------

    /**
     * Procède à l'évaluation d'un coup.
     *
     * @param choix
     * @return
     */
    public float evaluationCoup(String choix) {
        Grille clone = (Grille) this.grille.clone();
        boolean[] traduction = traductionDirection(choix);

        // Essaie d'effectuer le mouvement
        if (!clone.effectuerMouvement(traduction[0], traduction[1])) {
            return -1;
        }

        return evaluationGrille(clone.retournerCases());
    }

    /**
     * Evalue une grille
     *
     * @param cases
     * @return
     */
    public float evaluationGrille(Integer[][] cases) {
        float evaluation = 0;

        // Evaluation dimension 1
        for (int i = 0; i < cases.length; i++) {
            evaluation += evaluerCases(cases[i]);

        }
        // Evaluation dimension 2
        OperationMatricielle.transposerTableau(cases);
        for (int i = 0; i < cases.length; i++) {
            evaluation += evaluerCases(cases[i]);
        }

        float nombrePetiteCases = 0;

        // Calcul du nombre de petites cases
        for (int i = 0; i < cases.length; i++) {
            for (int j = 0; j < cases.length; j++) {
                if (cases[i][j] < 4 && cases[i][j] != -1) {
                    nombrePetiteCases++;
                }
            }
        }
        if (this.nbCoups == 0 || nombrePetiteCases == 0.0f) {
            return evaluation;
        }
        return evaluation * (nombrePetiteCases / this.nbCoups);
    }

    /**
     *
     * @param cases
     * @return la somme des évaluations d'un tableau de cases
     */
    private float evaluerCases(Integer[] cases) {
        float score = 0;
        for (int i = 0; i < cases.length - 1; i++) {
            score += evaluerDeuxCases(cases[i], cases[i + 1]);
        }

        return score;
    }

    /**
     * Procède à l'évaluation de deux cases
     *
     * @param i
     * @param j
     * @return un flotant positif, plus il est élevé, plus les cases participent
     * à une défaite rapide
     */
    private float evaluerDeuxCases(Integer i, Integer j) {

        // Constantes
        float alpha = 0.11f;
        float beta = 0.68f;
        float gamma = 1.75f;
        float caseVide = 2.0f;

        // Formules de calcul
        float fusionnable = beta / (i + j + 3);
        float egalite = i * alpha;
        float sansFusion = (i - j) * (i - j) * gamma;

        // Cases vides
        if (i == -1 && j == -1) {
            return caseVide;
        } // Cas fusionnable 
        else if (Math.abs(i - j) == 1 || (i == 0 & j == -1) || (i == -1 && j == 0)) {
            return fusionnable;
        } // Cas pas fusionnable et egale 
        else if (i.equals(j)) {
            return egalite;
        } // Cas sans fusion 
        else {
            return sansFusion;
        }
    }

    /**
     * Traduction des directions en un tableau de 2 booléens
     *
     * @param choix
     * @return
     */
    public boolean[] traductionDirection(String choix) {
        boolean[] tabTrad = new boolean[2];
        switch (choix) {
            case "GAUCHE":
                tabTrad[0] = false;
                tabTrad[1] = false;
                break;
            case "DROITE":
                tabTrad[0] = true;
                tabTrad[1] = false;
                break;
            case "HAUT":
                tabTrad[0] = false;
                tabTrad[1] = true;
                break;
            case "BAS":
                tabTrad[0] = true;
                tabTrad[1] = true;
                break;

        }
        return tabTrad;
    }
}
