
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.joueur;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Random;
import modele.Grille;

/**
 * Représente le joueur ordinateur qui effectue des coups aléatoires.
 *
 * @author Alex
 */
public class Ordinateur extends Joueur implements Serializable {

    protected static Random random = new Random();

    protected final String[] DIRECTION_ORDI = {"HAUT", "BAS", "GAUCHE", "DROITE"};

    /**
     * Constructeur du joueur ordinateur à partir d'une grille
     *
     * @param grille
     */
    public Ordinateur(Grille grille) {
        super(grille);
    }

    /**
     * Joue un coup de manière aléatoire
     */
    public void jouerUnCoup() {
        LinkedList<String> choixPossibles = new LinkedList<>();
        choixPossibles.addAll(Arrays.asList(DIRECTION_ORDI));

        Collections.shuffle(choixPossibles);

        /* tant que la liste des coups n'est pas vide 
        et que aucun coup n'a été effectué correctement
         */
        while (!choixPossibles.isEmpty()
                && !this.jouerUnCoup(choixPossibles.poll()));
    }

}
