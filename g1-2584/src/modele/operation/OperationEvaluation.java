/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.operation;

import modele.Case;
import modele.Grille;
import static modele.Parametres.TAILLE;

/**
 * Contient les opérations d'évaluation de la grille.
 *
 * @author mg
 */
public interface OperationEvaluation {

    /**
     * @param grille
     * @return vrai si la grille contient l'objectif
     */
    public static boolean estGagnee(Grille grille) {
        boolean gagnee = false;
        for (int i = 0; i < TAILLE && !gagnee; i++) {
            for (int j = 0; j < TAILLE && !gagnee; j++) {
                if (Case.estObjectif(grille.getCases()[i][j])) {
                    gagnee = true;
                }
            }
        }
        return gagnee;
    }

    /**
     * @param grille
     * @return vrai si la grille est complète
     */
    public static boolean estComplete(Grille grille) {
        boolean complet = true;
        for (int i = 0; i < TAILLE && complet; i++) {
            for (int j = 0; j < TAILLE && complet; j++) {
                if (Case.estVide(grille.getCases()[i][j])) {
                    complet = false;
                }
            }
        }
        return complet;
    }

    /**
     * @return vrai si aucun coup n'est possible
     */
    public static boolean estPerdue(Grille grille) {
        boolean coupPossible = false;

        // Evalue les coups pour chaque case
        for (int i = 0; i < TAILLE && !coupPossible; i++) {
            for (int j = 0; j < TAILLE && !coupPossible; j++) {
                // Si une case est vide, on peut effectuer un mouvement
                coupPossible = Case.estVide(grille.getCases()[i][j])
                        || evaluerFusionMultiDirections(i, j, grille);
            }
        }

        return !coupPossible;
    }

    /**
     * Vérifie si une fusion est faisable dans une des directions pour une case
     * donnée
     *
     * @param x coordonnée x de la case
     * @param y coordonnée y de la case
     * @return vrai si une fusion est possible
     */
    public static boolean evaluerFusionMultiDirections(int x, int y, Grille grille) {
        return evaluerFusionX(x, y, grille) || evaluerFusionY(x, y, grille);
    }

    /**
     * Vérifie si une fusion est faisable dans la dimention x pour une case
     * donnée
     *
     * @param x coordonnée x de la case
     * @param y coordonnée y de la case
     * @param grille
     * @return vrai si une fusion est possible
     */
    public static boolean evaluerFusionX(int x, int y, Grille grille) {
        if (x == 0) {
            return false;
        }
        return Case.sontFusionnables(grille.getCases()[x][y], grille.getCases()[x - 1][y]);
    }

    /**
     * Vérifie si une fusion est faisable dans la dimention y pour une case
     * donnée
     *
     * @param x coordonnée x de la case
     * @param y coordonnée y de la case
     * @param grille
     * @return vrai si une fusion est possible
     */
    public static boolean evaluerFusionY(int x, int y, Grille grille) {
        if (y == 0) {
            return false;
        }
        return Case.sontFusionnables(grille.getCases()[x][y], grille.getCases()[x][y - 1]);
    }

    /**
     *
     * @param grille
     * @return le rang maximum présent sur la grille
     */
    public static Integer retournerRangMaximum(Grille grille) {
        Case[][] tabCases = grille.getCases();
        Integer tampon = Integer.MIN_VALUE;
        for (int i = 0; i < tabCases.length; i++) {
            for (int j = 0; j < tabCases.length; j++) {
                if (tabCases[i][j].getRang() > tampon) {
                    tampon = tabCases[i][j].getRang();
                }
            }
        }
        return tampon;
    }
}
