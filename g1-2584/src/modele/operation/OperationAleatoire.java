/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.operation;

import java.util.Random;
import modele.Parametres;

/**
 * Interface qui contient la fonction de génération d'un rang
 *
 * @author mg
 */
public interface OperationAleatoire {

    static public Random random = new Random();

    /**
     * @return un rang aléatoire de la suite de fibonnacci, 0 ou bien 1.
     */
    public static int genererRangAleatoire() {
        return Parametres.PROBABILITE_RANG_APRES_MOUVEMENT[random.nextInt(
                Parametres.PROBABILITE_RANG_APRES_MOUVEMENT.length
        )];
    }

}
