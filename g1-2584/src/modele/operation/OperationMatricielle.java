/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.operation;

import modele.Case;

/**
 * interface qui contient les fonctions de transposition et renversement sur les
 * tableaux
 *
 * @author mg
 */
public interface OperationMatricielle {

    /**
     * Effectue un renversement sur tout le tableau passé en paramètre.
     *
     * @param tableau
     */
    public static void renverserTableau2D(Integer[][] tableau) {
        for (int i = 0; i < tableau.length; i++) {
            renverserTableau(tableau[i]);
        }
    }

    public static void renverserTableau(Integer[] tableau) {
        for (int i = 0; i < tableau.length / 2; i++) {
            tableau[i] += tableau[tableau.length - 1 - i];
            tableau[tableau.length - 1 - i] = tableau[i] - tableau[tableau.length - 1 - i];
            tableau[i] -= tableau[tableau.length - 1 - i];
        }
    }

    /**
     * Transpose le tableau passé en paramètre
     *
     * @param tableau
     */
    public static void transposerTableau(Integer[][] tableau) {
        for (int i = 0; i < tableau.length; i++) {
            for (int j = 0; j < i; j++) {
                tableau[i][j] += tableau[j][i];
                tableau[j][i] = tableau[i][j] - tableau[j][i];
                tableau[i][j] -= tableau[j][i];
            }
        }
    }

    /**
     * Effectue un renversement sur tout le tableau passé en paramètre.
     *
     * @param tableau tableau de cases
     */
    public static void renverserTableau2D(Case[][] tableau) {
        for (int i = 0; i < tableau.length; i++) {
            renverserTableau(tableau[i]);
        }
    }

    /**
     * Effectue un renversement sur tout le tableau passé en paramètre.
     *
     * @param tableau tableau de cases
     */
    public static void renverserTableau(Case[] tableau) {
        Case caseTemporaire;
        for (int i = 0; i < tableau.length / 2; i++) {
            caseTemporaire = tableau[i];
            tableau[i] = tableau[tableau.length - 1 - i];
            tableau[tableau.length - 1 - i] = caseTemporaire;
        }
    }

    /**
     * Transpose le tableau passé en paramètre
     *
     * @param tableau de cases
     */
    public static void transposerTableau(Case[][] tableau) {
        Case caseTemporaire;
        for (int i = 0; i < tableau.length; i++) {
            for (int j = 0; j < i; j++) {
                caseTemporaire = tableau[i][j];
                tableau[i][j] = tableau[j][i];
                tableau[j][i] = caseTemporaire;
            }
        }
    }
}
