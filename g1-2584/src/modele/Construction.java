/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import modele.joueur.Humain;
import modele.joueur.IntelligenceArtificielle;
import modele.joueur.Joueur;
import modele.joueur.Ordinateur;

/**
 * Gère tout la mécanique de construction d'une partie
 *
 * @author mg
 */
public class Construction implements Parametres {

    /**
     * Crée deux joueurs. Cette fonction prend en compte le type de joueur
     * souhaité par le biais des arguments.
     *
     * @param typeJoueurUn
     * @param typeJoueurDeux
     * @return un tableau de joueurs
     */
    private static Joueur[] creerDesJoueur(String typeJoueurUn,
            String typeJoueurDeux) {
        Grille[] grilles = creerDesGrilles();
        Joueur[] joueurs = new Joueur[2];
        joueurs[0] = creerUnJoueur(typeJoueurUn, grilles[0]);
        joueurs[1] = creerUnJoueur(typeJoueurDeux, grilles[1]);
        return joueurs;
    }

    /**
     * Renvoie un joueur en fonction du type du joueur demandé.
     *
     * @param typeJoueur
     * @param grille
     * @return un joueur
     */
    private static Joueur creerUnJoueur(String typeJoueur, Grille grille) {
        switch (typeJoueur) {
            default:
            case JOUEUR_HUMAIN:
                return new Humain(grille);
            case JOUEUR_ORDINATEUR:
                return new Ordinateur(grille);
            case JOUEUR_IA:
                return new IntelligenceArtificielle(grille);
        }
    }

//------------------------------------------------------------------------------
    /**
     * Crée une grille
     *
     * @return une grille
     */
    private static Grille creerUneGrille() {
        Grille grille = new Grille();
        grille.genererAleatoirementDeuxCellules();
        return grille;
    }

    /**
     * @return un tableau de deux grilles
     */
    private static Grille[] creerDesGrilles() {
        Grille[] retour = new Grille[2];
        retour[0] = creerUneGrille();
        retour[0].initialiserMouvement();
        retour[1] = (Grille) retour[0].clone();
        retour[0].initialiserMouvement();
        return retour;
    }

//------------------------------------------------------------------------------
    /**
     * Renvoie une partie en fonction du type des deux joueurs
     *
     * @param typeJoueurUn
     * @param typeJoueurDeux
     * @return une partie
     */
    public static Partie creerUnePartie(String typeJoueurUn,
            String typeJoueurDeux) {
        return new Partie(creerUnJeu(typeJoueurUn, typeJoueurDeux));
    }

    /**
     * Crée un jeu avec ses joueurs
     *
     * @param typeJoueurUn
     * @param typeJoueurDeux
     * @return un jeu
     */
    private static Jeu creerUnJeu(String typeJoueurUn, String typeJoueurDeux) {
        Joueur[] joueurs = creerDesJoueur(typeJoueurUn, typeJoueurDeux);
        return new Jeu(joueurs[0], joueurs[1]);
    }

//------------------------------------------------------------------------------
    /**
     * Crée un état en fonction de la chaine passé en paramètre.
     *
     * @param typeEtat
     * @return un etat
     */
    public static Etat creerUnEtat(String typeEtat) {
        Etat etatRetour;

        switch (typeEtat) {
            case ETAT_NON_TERMINE:
                etatRetour = new Etat(false, false);
                break;
            case ETAT_PERDU:
                etatRetour = new Etat(true, false);
                break;
            case ETAT_GAGNE:
                etatRetour = new Etat(false, true);
                break;

            default:
                etatRetour = new Etat(false, false);
                break;
        }
        return etatRetour;
    }

}
