/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.deplacement;

import modele.Case;

/**
 * Déplacement de case, ainsi que les méthodes nécessaires à l'analyse de case
 * pour trouver le mouvement correspondant
 *
 * @author emarquer
 */
public interface Deplacement {

    /**
     * Analyse une case et crée le déplacement correspondant
     *
     * @param uneCase case à analyser
     * @return déplacement correspondant à la case
     */
    public static Deplacement analyserCase(Case uneCase) {
        // Cas d'une case vide
        if (Case.estVide(uneCase)) {

            return null;

        } else {
            Deplacement deplacement;

            // Recuperation des eventuels parents de la case
            Case parentUn = uneCase.getParentUn();
            Case parentDeux = uneCase.getParentDeux();

            // Si la case n'a pas de parents
            if (Case.estVide(parentUn) || Case.estVide(parentUn)) {
                // Cas d'une case qui vient d'apparaître
                if (uneCase.isNouvelle()) {
                    deplacement = creerApparition(uneCase);

                    // Cas d'une case qui a uniquement bougé ou qui n'a pas bougé
                } else {
                    deplacement = creerTranslation(uneCase);
                }

                // Si la case a des parents, c'est quelle est issue d'une fusion
            } else {
                deplacement = creerFusion(parentUn, parentDeux, uneCase);
            }

            uneCase.setNouvelle(false);

            return deplacement;
        }
    }

    /**
     * Crée une apparition à partir d'une case
     *
     * @param uneCase case à analyser
     * @return apparition de la case
     */
    public static Deplacement creerApparition(Case uneCase) {
        return new Apparition(
                uneCase.getNouveauX(), uneCase.getNouveauY(),
                uneCase.getRang());
    }

    /**
     * Crée une translation à partir d'une case
     *
     * @param uneCase case à analyser
     * @return translation de la case
     */
    public static Deplacement creerTranslation(Case uneCase) {
        return new Translation(
                uneCase.getAncienX(), uneCase.getAncienY(),
                uneCase.getNouveauX(), uneCase.getNouveauY(),
                uneCase.getRang());
    }

    /**
     * Crée une fusio à partir d'une case et de ses parents
     *
     * @param parentUn premier parent de la case
     * @param parentDeux deuxieme parent de la case
     * @param uneCase case à analyser
     * @return translation de la case
     */
    public static Deplacement creerFusion(Case parentUn, Case parentDeux,
            Case uneCase) {
        Fusion fusion = new Fusion(
                parentUn.getAncienX(), parentUn.getAncienY(),
                parentDeux.getAncienX(), parentDeux.getAncienY(),
                uneCase.getNouveauX(), uneCase.getNouveauY(),
                parentUn.getRang(), parentDeux.getRang(), uneCase.getRang());
        uneCase.nettoyerReferencesParents();
        return fusion;
    }
}
