/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.deplacement;

import java.io.Serializable;
import java.util.ArrayList;
import modele.Case;
import modele.Parametres;

/**
 * Ensemble de déplacements des cases d'une grille
 *
 * @author emarquer
 */
public class Deplacements extends ArrayList<Deplacement> implements Parametres, Serializable {

    private Deplacements() {
        super();
    }

    /**
     * Initialise les cases à leur position actuelle
     *
     * @param grille cases à initialiser
     */
    public static void initialiserPositions(Case[][] grille) {
        for (int i = 0; i < TAILLE; i++) {
            for (int j = 0; j < TAILLE; j++) {
                grille[i][j].changerAnciennePosition(i, j);
            }
        }
    }

    /**
     * Marque toutes les cases comme anciennes
     *
     * @param grille cases à marquer comme anciennes
     */
    public static void changerAncien(Case[][] grille) {
        for (int i = 0; i < TAILLE; i++) {
            for (int j = 0; j < TAILLE; j++) {
                grille[i][j].setNouvelle(false);
            }
        }
    }

    /**
     * Initialise les cases à leur position actuelle
     *
     * @param grille cases à initialiser
     */
    public static void miseAJoursPositions(Case[][] grille) {
        for (int i = 0; i < TAILLE; i++) {
            for (int j = 0; j < TAILLE; j++) {
                grille[i][j].changerNouvellePosition(i, j);
            }
        }
    }

    /**
     * Crée tous les déplacements des cases de la grille la grille
     *
     * @param grille cases à analyser
     * @return les déplacements des cases
     */
    public static Deplacements genererDeplacements(Case[][] grille) {
        Deplacements deplacements = new Deplacements();

        for (Case[] cases : grille) {
            for (Case uneCase : cases) {
                /* Si la case n'est pas vide, on l'analyse et on ajoute le
                 * mouvement resultant aux mouvements */
                if (!Case.estVide(uneCase)) {
                    deplacements.add(Deplacement.analyserCase(uneCase));
                }
            }
        }

        return deplacements;
    }
}
