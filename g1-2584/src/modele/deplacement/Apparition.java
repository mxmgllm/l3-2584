/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.deplacement;

import java.io.Serializable;

/**
 * Apparition d'une case à un point donné
 *
 * @author emarquer
 */
public class Apparition implements Deplacement, Serializable {

    int x, y;
    int rang;

    /**
     * Crée une nouvelle apparition de case
     *
     * @param x coordonnée x de départ de la nouvelle case
     * @param y coordonnée x de départ de la nouvelle case
     * @param rang rang dans la suite de la nouvelle case
     */
    public Apparition(int x, int y, int rang) {
        this.x = x;
        this.y = y;
        this.rang = rang;
    }

    /**
     * @return la coordonnée x de la case
     */
    public int getX() {
        return x;
    }

    /**
     * @return la coordonnée y de la case
     */
    public int getY() {
        return y;
    }

    /**
     * @return le rang dans la suite de la case
     */
    public int getRang() {
        return rang;
    }
}
