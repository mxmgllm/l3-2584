/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.deplacement;

import java.io.Serializable;

/**
 * Déplacement d'une case d'un point à un autre
 *
 * @author emarquer
 */
public class Translation implements Deplacement, Serializable {

    private int xDepart, yDepart, xArrivee, yArrivee;
    private int rang;

    /**
     * Crée une nouvelle translation de case
     *
     * @param xDepart coordonnée x de départ
     * @param yDepart coordonnée x de départ
     * @param xArrivee la coordonnée x d'arrivée
     * @param yArrivee la coordonnée x d'arrivée
     * @param rang le rang dans la suite de la case
     */
    public Translation(int xDepart, int yDepart, int xArrivee, int yArrivee, int rang) {
        this.xDepart = xDepart;
        this.yDepart = yDepart;
        this.xArrivee = xArrivee;
        this.yArrivee = yArrivee;
        this.rang = rang;
    }

    /**
     * @return la coordonnée x de départ
     */
    public int getXDepart() {
        return xDepart;
    }

    /**
     * @return la coordonnée y de départ
     */
    public int getYDepart() {
        return yDepart;
    }

    /**
     * @return la coordonnée x d'arrivée
     */
    public int getXArrivee() {
        return xArrivee;
    }

    /**
     * @return la coordonnée y d'arrivée
     */
    public int getYArrivee() {
        return yArrivee;
    }

    /**
     * @return le rang dans la suite de la case
     */
    public int getRang() {
        return rang;
    }

//------------------------------------------------------------------------------
    /**
     * @return la différence entre la coordonnée x d'arrivée et celle de départ
     */
    public int retournerDifferenceX() {
        return this.getXArrivee() - this.getXDepart();
    }

    /**
     * @return la différence entre la coordonnée y d'arrivée et celle de départ
     */
    public int retournerDifferenceY() {
        return this.getYArrivee() - this.getYDepart();
    }
}
