/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.deplacement;

import java.io.Serializable;

/**
 * Mouvement particulier composé du déplacement de deux cases au même endroit,
 * et de l'apparition d'une nouvelle case au point d'arrivée
 *
 * @author emarquer
 */
public class Fusion implements Deplacement, Serializable {

    private Translation deplacementParentUn, deplacementParentDeux;
    private Apparition apparitionFusion;

    /**
     * Crée une fusion à partir du point de départ des deux cases parentes et du
     * point d'arrivée de ces cases, servant de point d'apparition de la case
     * résultante.
     *
     * @param xDepartParentUn coordonnée x de départ du parent un
     * @param yDepartParentUn coordonnée y de départ du parent un
     * @param xDepartParentDeux coordonnée x de départ du parent deux
     * @param yDepartParentDeux coordonnée y de départ du parent deux
     * @param xArrivee coordonnée x d'arrivée de la case fusionnée
     * @param yArrivee coordonnée y d'arrivée de la case fusionnée
     * @param rangParentUn rang dans la suite du parent un
     * @param rangParentDeux rang dans la suite du parent deux
     * @param rangFusion rang dans la suite de la case fusionnée
     */
    public Fusion(int xDepartParentUn, int yDepartParentUn,
            int xDepartParentDeux, int yDepartParentDeux,
            int xArrivee, int yArrivee,
            int rangParentUn, int rangParentDeux, int rangFusion) {
        this.deplacementParentUn
                = new Translation(xDepartParentUn, yDepartParentUn,
                        xArrivee, yArrivee,
                        rangParentUn);
        this.deplacementParentDeux
                = new Translation(xDepartParentDeux, yDepartParentDeux,
                        xArrivee, yArrivee,
                        rangParentDeux);
        this.apparitionFusion = new Apparition(xArrivee, yArrivee, rangFusion);
    }

    /**
     * @return l'apparition de la tuile résultat
     */
    public Apparition getApparitionFusion() {
        return apparitionFusion;
    }

//------------------------------------------------------------------------------    
    /**
     * @return le déplacement du parent un
     */
    public Translation getDeplacementParentUn() {
        return deplacementParentUn;
    }

    /**
     * @return le déplacement du parent deux
     */
    public Translation getDeplacementParentDeux() {
        return deplacementParentDeux;
    }

}
