/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.bdd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Sylvain Castagnos Classe utilisée pour établir une connexion avec la
 * base de données, interroger la base et insérer de nouveaux tuples dans la
 * base
 */
public class ConnexionBDD implements ParametreBDD {

    private String host, port, dbname, username, password;
    private Connection con = null;

    /**
     * Construction par défaut avec les constantes présente dans ParametreBDD
     */
    public ConnexionBDD() {
        this.host = HOTE;
        this.port = PORT;
        this.dbname = NOM_BASE_DONNEE;
        this.username = NOM_UTILISATEUR;
        this.password = MOT_DE_PASSE;
    }

//------------------------------------------------------------------------------    
    /**
     * Ouvre la connexion avec la base de données
     */
    private void ouvrirConnexion() {
        String connectUrl = "jdbc:mysql://" + this.host + ":" + this.port + "/" + this.dbname;
        if (con != null) {
            this.fermerConnexion();
        }
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            con = DriverManager.getConnection(connectUrl, username, password);
            System.out.println("Database connection established.");
        } catch (ClassNotFoundException cnfe) {
            System.err.println("Cannot load db driver: com.mysql.jdbc.Driver");
        } catch (Exception e) {
            System.err.println("Erreur inattendue");
        }
    }

    /**
     * Ferme la connexion avec la base de données
     */
    private void fermerConnexion() {
        if (con != null) {
            try {
                con.close();
                System.out.println("Database connection terminated.");
            } catch (Exception e) {
                /* ignore close errors */ }
        }
    }

//------------------------------------------------------------------------------
    /**
     * Interroge la base de données avec la requête passée en paramètre et
     * retourne les résultats sous forme d'une liste de String. Il faut utiliser
     * la méthode executeQuery dans la classe Statement (voir cours 12). Indice
     * : comme on ne sait pas à l'avance combien d'attributs (colonnes) on a
     * dans nos tuples, on peut utiliser la classe ResultSetMetaData (voir
     * méthodes getMetaData() de la classe ResultSet et getColumnCount() de la
     * classe ResultSetMetaData)
     */
    public ArrayList<String> recupererTuples(String query) {
        ArrayList<String> res = null;
        try {
            this.ouvrirConnexion();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            ResultSetMetaData metadata = rs.getMetaData(); // permet de récupérer les noms des colonnes des tuples en sortie de la requête
            String tuple;
            res = new ArrayList<>();
            while (rs.next()) {
                tuple = "";
                for (int i = 1; i <= metadata.getColumnCount(); i++) {
                    tuple += rs.getString(i);
                    if (i < metadata.getColumnCount()) {
                        tuple += ";";
                    }
                }
                res.add(tuple);
            }
            stmt.close();
        } catch (SQLException e) {
            System.err.println("Probleme avec la requete");
        } finally {
            this.fermerConnexion();
        }
        return res;
    }

    /**
     * Insère un ou plusieurs tuples dans la base à partir de la requête passée
     * en paramètre Pour cela, il faut utiliser la méthode executeUpdate dans la
     * classe Statement
     */
    public void insererTuples(String updateQuery) {
        try {
            this.ouvrirConnexion();
            Statement stmt = con.createStatement();
            int n = stmt.executeUpdate(updateQuery);
            stmt.close();
        } catch (SQLException e) {

        } finally {
            this.fermerConnexion();
        }
    }

}
