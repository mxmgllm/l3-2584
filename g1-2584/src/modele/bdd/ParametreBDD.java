/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.bdd;

/**
 * Paramètres par défaut pour la connexion avec la BDD.
 *
 * @author mg
 */
public interface ParametreBDD {

    static final String HOTE = "mysql-g1-2584.alwaysdata.net";
    static final String PORT = "3306";
    static final String NOM_UTILISATEUR = "g1-2584_u";
    static final String NOM_BASE_DONNEE = "g1-2584_jeux_2584";
    static final String MOT_DE_PASSE = "2584";

}
