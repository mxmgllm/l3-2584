/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.bdd;

import java.text.MessageFormat;
import java.util.ArrayList;
import modele.Partie;

/**
 * Représente la table de donnée partie.
 *
 * @author mg
 */
public class PartieBDD {

    static ConnexionBDD connexion = new ConnexionBDD();
    private static final Integer NOMBRE_ATTRIBUTS = 8;
    private Integer id,
            scoreUn,
            scoreDeux,
            nbCoupUn,
            nbCoupDeux,
            tuileMaxUn,
            tuileMaxDeux;
    private String date;

    //TODO
    public PartieBDD(Partie partie) {
        scoreUn = partie.getJeu().retournerScoreUn();
        scoreDeux = partie.getJeu().retournerScoreDeux();
        nbCoupUn = partie.getJeu().retournerNbCoupUn();
        nbCoupDeux = partie.getJeu().retournerNbCoupDeux();
        tuileMaxUn = partie.getJeu().retournerRangMaxJoueurUn();
        tuileMaxDeux = partie.getJeu().retournerRangMaxJoueurDeux();
        id = null;
        date = null;
    }

    public PartieBDD(Integer id, Integer scoreUn,
            Integer scoreDeux,
            Integer nbCoupUn,
            Integer nbCoupDeux,
            Integer tuileMaxUn,
            Integer tuileMaxDeux,
            String date) {
        this.id = id;
        this.scoreUn = scoreUn;
        this.scoreDeux = scoreDeux;
        this.nbCoupUn = nbCoupUn;
        this.nbCoupDeux = nbCoupDeux;
        this.tuileMaxUn = tuileMaxUn;
        this.tuileMaxDeux = tuileMaxDeux;
        this.date = date;
    }
//------------------------------------------------------------------------------    

    /**
     *
     * @return une liste de partieBDD
     */
    static public ArrayList<PartieBDD> selectionnerParties() {
        String requete = "SELECT * FROM `Partie` ORDER BY `date` DESC LIMIT 10;";
        return convertirListeTupleEnPartie(connexion.recupererTuples(requete));

    }

    /**
     * Converti les tuples en parties
     *
     * @param tuples
     * @return une liste de partieBDD
     */
    static private ArrayList<PartieBDD> convertirListeTupleEnPartie(
            ArrayList<String> tuples) {
        ArrayList<PartieBDD> conversion = new ArrayList<>();
        for (String element : tuples) {
            conversion.add(convertirTupleEnPartie(element));
        }
        return conversion;

    }

    /**
     * Implémentation naive de la conversion d'un tuple en partie
     *
     * @deprecated
     * @param partieChaine
     * @return
     */
    static private PartieBDD convertirTupleEnPartie(String partieChaine) throws NumberFormatException {
        String[] attributs = partieChaine.split(";");
        if (attributs.length < NOMBRE_ATTRIBUTS) {
            return null;
        } else {
            return new PartieBDD(Integer.parseInt(attributs[0]),
                    Integer.parseInt(attributs[1]),
                    Integer.parseInt(attributs[2]),
                    Integer.parseInt(attributs[3]),
                    Integer.parseInt(attributs[4]),
                    Integer.parseInt(attributs[5]),
                    Integer.parseInt(attributs[6]),
                    attributs[7]);
        }
    }

//------------------------------------------------------------------------------
    /**
     * Insere la partie en bdd
     *
     * @param Partie
     */
    public static void insererPartie(PartieBDD Partie) {
        connexion.insererTuples(PartieBDD.creationRequeteInsertion(Partie));
    }

    /**
     *
     * @param partie
     * @return la requete d'insertion d'une nouvelle partie
     */
    private static String creationRequeteInsertion(PartieBDD partie) {
        String requete = "INSERT INTO `Partie` "
                + "(`id`,"
                + " `score_joueur_un`,"
                + " `score_joueur_deux`,"
                + " `nombre_coups_joueur_un`,"
                + " `nombre_coups_joueur_deux`,"
                + " `tuile_maximum_joueur_un`,"
                + " `tuile_maximum_joueur_deux`,"
                + " `date`) VALUES"
                + " ("
                + "NULL,"
                + " ''{0}'',"
                + " ''{1}'',"
                + " ''{2}'',"
                + " ''{3}'',"
                + " ''{4}'',"
                + " ''{5}'',"
                + " NOW()"
                + ")"
                + ";";
        String requeteFormat = MessageFormat.format(requete,
                partie.getScoreUn(),
                partie.getScoreDeux(),
                partie.getNbCoupUn(),
                partie.getNbCoupDeux(),
                partie.getTuileMaxUn(),
                partie.getTuileMaxDeux()
        );
        return requeteFormat;
    }

//------------------------------------------------------------------------------
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @return the scoreUn
     */
    public Integer getScoreUn() {
        return scoreUn;
    }

    /**
     * @return the scoreDeux
     */
    public Integer getScoreDeux() {
        return scoreDeux;
    }

    /**
     * @return the nbCoupUn
     */
    public Integer getNbCoupUn() {
        return nbCoupUn;
    }

    /**
     * @return the nbCoupDeux
     */
    public Integer getNbCoupDeux() {
        return nbCoupDeux;
    }

    /**
     * @return the tuileMaxUn
     */
    public Integer getTuileMaxUn() {
        return tuileMaxUn;
    }

    /**
     * @return the tuileMaxDeux
     */
    public Integer getTuileMaxDeux() {
        return tuileMaxDeux;
    }

    /**
     * @return the date
     */
    public String getDate() {
        return date;
    }

}
