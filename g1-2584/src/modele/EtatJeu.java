/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

/**
 * Représente l'état du jeu
 *
 * @author mg
 */
public class EtatJeu implements Parametres {

    private Etat[] partie = new Etat[2];

    /**
     * Construction d'un état du jeu à partir de l'état des 2 joueurs
     *
     * @param etatUn etat joueur 1
     * @param etatDeux etat joueur 2
     */
    public EtatJeu(Etat etatUn, Etat etatDeux) {
        partie[0] = etatUn;
        partie[1] = etatDeux;
    }

    /**
     *
     * @return une chaine qui représente le gagnant (joueur1 ou joueur2),
     * l'égalite ou le fait que la partie ne soit pas terminée
     */
    public String retournerGagnant() {
        if ((partie[0].isVictoire() && partie[1].isVictoire()) || (partie[0].retournerPerdu() && partie[1].retournerPerdu())) {
            return EGALITE;
        } else if (partie[0].isVictoire() || partie[1].retournerPerdu()) {
            return JOUEUR_UN;
        } else if (partie[0].retournerPerdu() || partie[1].isVictoire()) {
            return JOUEUR_DEUX;
        } else {
            return ETAT_NON_TERMINE;
        }
    }

}
