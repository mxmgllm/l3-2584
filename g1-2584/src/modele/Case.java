/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import java.io.Serializable;

/**
 * Classe de case, possédant un rang dans la suite, des coordonnées, et des
 * mouvements. Gère les interactions entre les cases,
 *
 * @author emarquer
 */
public class Case implements Parametres, Cloneable, Serializable {

    static Case VIDE = new Case(-1);

    /**
     * @param uneCase case a évaluer
     * @return vrai si la case est nulle ou si son rang est négatif
     */
    public static boolean estVide(Case uneCase) {
        return uneCase == null || uneCase.getRang() < 0;
    }

    /**
     * @param uneCase case a évaluer
     * @return vrai si le rang de la case est égal a celui de l'objectif
     */
    public static boolean estObjectif(Case uneCase) {
        return !estVide(uneCase) && uneCase.getRang().equals(OBJECTIF);
    }

    /**
     * Evalue la fusion de deux cases
     *
     * @param uneCase premiere case à évaluer
     * @param autreCase seconde case à évaluer
     * @return vrai si les cases sont fusionnables
     */
    public static boolean sontFusionnables(Case uneCase, Case autreCase) {
        // Si une des deux cases est vides, on ne peut pas fusionner
        if (estVide(uneCase) || estVide(autreCase)) {
            return false;

            // Si les deux cases ont le rang 0, on peut les fusionner
        } else if (uneCase.getRang() == 0 && autreCase.getRang() == 0) {
            return true;

            // Si les deux cases ont un equart de rang de 1, on peut les fusionner
        } else if (Math.abs(uneCase.getRang() - autreCase.getRang()) == 1) {
            return true;
        }

        // Dans tous les autres cas, on ne peut pas fusionner les cases
        return false;
    }

    /**
     * Evalue l'égalité de deux cases
     *
     * @param uneCase premiere case à évaluer
     * @param autreCase seconde case à évaluer
     * @return vrai si les cases sont "gales
     */
    public static boolean sontEgales(Case uneCase, Case autreCase) {
        // Si les deux cases sont vides, elles sont égales
        if (estVide(uneCase) && estVide(autreCase)) {
            return true;

            // Si une seule des deux cases est vides, elles sont différentes
        } else if (estVide(uneCase) || estVide(autreCase)) {
            return false;

            // Si les deux cases ont le même rang, , elles sont égales
        } else if (uneCase.getRang() == autreCase.getRang()) {
            return true;
        }

        // Dans tous les autres cas, les cases sont différentes
        return false;
    }

//------------------------------------------------------------------------------
    private Integer rang;
    private boolean nouvelle;
    private Case parentUn, parentDeux;
    private int nouveauX, nouveauY, ancienX, ancienY;

    /**
     * Construit une nouvelle case
     *
     * @param rang rang dans la suite
     */
    public Case(Integer rang) {
        this.rang = rang;
        this.parentUn = null;
        this.parentDeux = null;
        this.nouvelle = true;

        // Initialisation des coordonnées à des valeurs absurdes
        this.nouveauX = this.nouveauY = this.ancienX = this.ancienY = -1;
    }

    /**
     * Construit une nouvelle case a partir de deux autres cases
     *
     * @param parentUn case parente une
     * @param parentDeux case parente deux
     */
    public Case(Case parentUn, Case parentDeux) {
        this(Math.max(parentUn.getRang(), parentDeux.getRang()) + 1);
        this.parentUn = parentUn;
        this.parentDeux = parentDeux;
    }

    /**
     * @return le rang dans la suite
     */
    public Integer getRang() {
        return rang;
    }

    /**
     *
     * @return un clone
     */
    @Override
    protected Object clone() {
        Case caseClone = new Case(getRang());
        caseClone.nouvelle = this.nouvelle;
        return caseClone;
    }

    /**
     * Fusionne deux cases et crée une nouvelle case contenant le resultat de la
     * fusion
     *
     * @param autreCase
     * @return une nouvelle case à partir de la fusion
     */
    public Case fusionner(Case autreCase) {
        return new Case(this, autreCase);
    }

    /**
     * Initialise la position de départ de la case
     *
     * @param x coordonnée x de la case
     * @param y coordonnée y de la case
     */
    public void changerAnciennePosition(int x, int y) {
        this.ancienX = x;
        this.ancienY = y;
    }

    /**
     * Initialise la position d'arrivée de la case
     *
     * @param x coordonnée x de la case
     * @param y coordonnée y de la case
     */
    public void changerNouvellePosition(int x, int y) {
        this.nouveauX = x;
        this.nouveauY = y;
    }

    /**
     * Supprime les références de la case à ses parents
     */
    public void nettoyerReferencesParents() {
        this.parentUn = null;
        this.parentDeux = null;
    }

    /**
     * @return vraai si la case est nouvelle
     */
    public boolean isNouvelle() {
        return nouvelle;
    }

    /**
     * @param nouvelle faux si la case n'est plus nouvelle
     */
    public void setNouvelle(boolean nouvelle) {
        this.nouvelle = nouvelle;
    }

    /**
     * @return premier parent de la case
     */
    public Case getParentUn() {
        return parentUn;
    }

    /**
     * @return deuxieme parent de la case
     */
    public Case getParentDeux() {
        return parentDeux;
    }

    /**
     * @return nouvelle coordonnée x
     */
    public int getNouveauX() {
        return nouveauX;
    }

    /**
     * @return nouvelle coordonnée y
     */
    public int getNouveauY() {
        return nouveauY;
    }

    /**
     * @return ancienne coordonnée x
     */
    public int getAncienX() {
        return ancienX;
    }

    /**
     * @return ancienne coordonnée y
     */
    public int getAncienY() {
        return ancienY;
    }
}
