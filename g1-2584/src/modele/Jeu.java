/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import java.io.Serializable;
import modele.deplacement.Deplacements;
import modele.joueur.Humain;
import modele.joueur.IntelligenceArtificielle;
import modele.joueur.Joueur;
import modele.joueur.Ordinateur;
import modele.operation.OperationEvaluation;

/**
 * Classe qui représente le jeu 2584
 *
 * @author mg
 */
public class Jeu implements Serializable {

    private Joueur joueurUn;
    private Joueur joueurDeux;

    /**
     * Construction du jeu
     *
     * @param joueurUn
     * @param joueurDeux
     */
    public Jeu(Joueur joueurUn, Joueur joueurDeux) {
        this.joueurUn = joueurUn;
        this.joueurDeux = joueurDeux;
    }

    /**
     *
     * @return l'état du jeu
     */
    public EtatJeu retournerEtatJeu() {
        return new EtatJeu(joueurUn.retournerObjectifAtteint(),
                joueurDeux.retournerObjectifAtteint());
    }

//------------------------------------------------------------------------------
    /**
     * @return le nom de la classe réelle du joueur un
     */
    public String retournerNomClasseJoueurUn() {
        return Joueur.retournerClasseJoueur(joueurUn);
    }

    /**
     * @return le nom de la classe réelle du joueur deux
     */
    public String retournerNomClasseJoueurDeux() {
        return Joueur.retournerClasseJoueur(joueurDeux);
    }

//------------------------------------------------------------------------------
    /**
     * Applique un mouvement pour le joueur un ordinateur
     */
    public void jouerUn() {
        if (joueurUn instanceof IntelligenceArtificielle) {
            IntelligenceArtificielle joueur = (IntelligenceArtificielle) joueurUn;
            joueur.jouerUnCoup();
        } else {
            Ordinateur joueur = (Ordinateur) joueurUn;
            joueur.jouerUnCoup();
        }
    }

    /**
     * Applique un mouvement pour le joueur deux ordinateur
     */
    public void jouerDeux() {
        if (joueurDeux instanceof IntelligenceArtificielle) {
            IntelligenceArtificielle joueur = (IntelligenceArtificielle) joueurDeux;
            joueur.jouerUnCoup();
        } else {
            Ordinateur joueur = (Ordinateur) joueurDeux;
            joueur.jouerUnCoup();
        }
    }

//------------------------------------------------------------------------------
    /**
     * Applique un mouvement pour le joueur un humain
     *
     * @param mouvement
     * @return vrai si mouvement effectué
     */
    public boolean jouerUn(String mouvement) {
        return joueurUn.jouerUnCoup(mouvement);
    }

    /**
     * Applique un mouvement pour le joueur un humain
     *
     * @param mouvement
     * @return vrai si mouvement effectué
     */
    public boolean jouerDeux(String mouvement) {
        return joueurDeux.jouerUnCoup(mouvement);
    }

//------------------------------------------------------------------------------    
    /**
     * annule le coup du joueur 1 si il est humain et si c'est possible
     *
     * @return vraie si l'annulation est effectué
     */
    public boolean annulerCoupUn() {
        if (this.joueurUn instanceof Humain) {
            Humain joueur = (Humain) this.joueurUn;
            return joueur.annulerCoup();
        }
        return false;
    }

    /**
     * annule le coup du joueur 2 si il est humain et si c'est possible
     *
     * @return vraie si l'annulation est effectué
     */
    public boolean annulerCoupDeux() {
        if (this.joueurDeux instanceof Humain) {
            Humain joueur = (Humain) this.joueurDeux;
            return joueur.annulerCoup();
        }
        return false;
    }

//------------------------------------------------------------------------------    
    /**
     *
     * @return le score du joueur un
     */
    public int retournerScoreUn() {
        return joueurUn.retournerScore();
    }

    /**
     *
     * @return le score du joueur deux
     */
    public int retournerScoreDeux() {
        return joueurDeux.retournerScore();
    }

//------------------------------------------------------------------------------    
    /**
     *
     * @return le nombre de coup du joueur un
     */
    public int retournerNbCoupUn() {
        return this.joueurUn.getNbCoups();
    }

    /**
     *
     * @return le nombre de coups du joueur deux
     */
    public int retournerNbCoupDeux() {
        return this.joueurDeux.getNbCoups();
    }

//------------------------------------------------------------------------------    
    /**
     *
     * @return la grille du joueur un
     */
    public Integer[][] retournerGrilleValeurUne() {
        return joueurUn.retournerCases();
    }

    /**
     *
     * @return la grille du joueur deux
     */
    public Integer[][] retournerGrilleValeurDeux() {
        return joueurDeux.retournerCases();
    }

//------------------------------------------------------------------------------    
    /**
     * @return les derniers déplacements de cases du joueur un
     */
    public Deplacements retournerDeplacementsUn() {
        return joueurUn.retournerDeplacements();
    }

    /**
     * @return les derniers déplacements de cases du joueur deux
     */
    public Deplacements retournerDeplacementsDeux() {
        return joueurDeux.retournerDeplacements();
    }

//------------------------------------------------------------------------------    
    /**
     * Prend en compte l'existance d'un tour précédent et le nombre
     * d'annulations restantes pour déterminer la possibiliter d'annuler
     *
     * @return vrai si le joueur 1 peut annuler un coup
     */
    public boolean peutAnnulerCoupUn() {
        if (this.joueurUn instanceof Humain) {
            Humain joueur = (Humain) this.joueurUn;
            return joueur.peutAnnulerUnCoup();
        }
        return false;
    }

    /**
     * Prend en compte l'existance d'un tour précédent et le nombre
     * d'annulations restantes pour déterminer la possibiliter d'annuler
     *
     * @return vrai si le joueur 2 peut annuler un coup
     */
    public boolean peutAnnulerCoupDeux() {
        if (this.joueurDeux instanceof Humain) {
            Humain joueur = (Humain) this.joueurDeux;
            return joueur.peutAnnulerUnCoup();
        }
        return false;
    }

//------------------------------------------------------------------------------    
    /**
     * @return le nombre d'annulations restantes du joueur 1
     */
    public int retournerCompteurAnnulationUn() {
        if (this.joueurUn instanceof Humain) {
            Humain joueur = (Humain) this.joueurUn;
            return joueur.getCompteurAnnulation();
        }
        return 0;
    }

    /**
     * @return le nombre d'annulations restantes du joueur 2
     */
    public int retournerCompteurAnnulationDeux() {
        if (this.joueurDeux instanceof Humain) {
            Humain joueur = (Humain) this.joueurDeux;
            return joueur.getCompteurAnnulation();
        }
        return 0;
    }

//------------------------------------------------------------------------------    
    /**
     *
     * @return le rang maximum du joueur un
     */
    public Integer retournerRangMaxJoueurUn() {
        return OperationEvaluation.retournerRangMaximum(this.joueurUn.getGrille());
    }

    /**
     *
     * @return le rang maximum du joueur deux
     */
    public Integer retournerRangMaxJoueurDeux() {
        return OperationEvaluation.retournerRangMaximum(this.joueurDeux.getGrille());
    }

}
