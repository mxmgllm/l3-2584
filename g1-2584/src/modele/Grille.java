package modele;

import modele.operation.OperationMatricielle;
import modele.operation.OperationEvaluation;
import modele.operation.OperationAleatoire;
import java.io.Serializable;
import modele.deplacement.Deplacements;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;

/**
 * Grille contenant des cases, gérant les mouvements, et quelques opérations
 * matricielles
 */
public class Grille implements Parametres, Cloneable, Serializable {

    private Deplacements derniersDeplacements;
    private Case[][] cases;
    private int score;

    /**
     * Construction d'une grille.
     */
    public Grille() {
        cases = new Case[TAILLE][TAILLE];
        this.score = 0;
        initialiserGrilleVide();
    }

    /**
     * Créé une grille à partir d'un tableau de rangs de cases
     *
     * @param cases tableau de rangs des cases de la grille à créer
     */
    public Grille(Integer[][] cases) {
        this.cases = new Case[TAILLE][TAILLE];

        // Création des cases a partir du tableau de rangs
        for (int i = 0; i < TAILLE; i++) {
            for (int j = 0; j < TAILLE; j++) {
                //this.cases[i][j] = new Case(cases[i][j], i, j);
                this.cases[i][j] = new Case(cases[i][j]);
            }
        }
    }

    /**
     * @return un clone de la grille avec un score à zéro par mesure de sécurité
     */
    @Override
    public Object clone() {
        Grille clone = new Grille();

        for (int i = 0; i < TAILLE; i++) {
            for (int j = 0; j < TAILLE; j++) {
                clone.cases[i][j] = (Case) this.getCases()[i][j].clone();
            }
        }
        clone.score = 0;
        return clone;
    }

    /**
     * @return les cases de la grille
     */
    public Case[][] getCases() {
        return cases;
    }

    /**
     *
     * @return un tableau 2d des rangs présents sur la grille
     */
    public Integer[][] retournerCases() {
        Integer[][] rangsCases = new Integer[TAILLE][TAILLE];

        // Extraction des rangs des cases de la grille
        for (int i = 0; i < TAILLE; i++) {
            for (int j = 0; j < TAILLE; j++) {
                if (Case.estVide(this.getCases()[i][j])) {
                    rangsCases[i][j] = -1;
                } else {
                    rangsCases[i][j] = this.getCases()[i][j].getRang();
                }
            }
        }

        return rangsCases;
    }

    /**
     * @return les derniers déplacements
     */
    public Deplacements getDerniersDeplacements() {
        return derniersDeplacements;
    }

    /**
     * Effectue un mouvement sur la grille. Si le mouvement n'est pas possible
     * la grille ne bouge pas
     *
     * @param sensDeLecture
     * @param vertical
     * @return vrai si un mouvement à été effectué
     */
    public boolean effectuerMouvement(boolean sensDeLecture, boolean vertical) {

        Deplacements.initialiserPositions(this.getCases());

        if (vertical) {
            OperationMatricielle.transposerTableau(this.getCases());
        }
        if (sensDeLecture) {
            OperationMatricielle.renverserTableau2D(this.getCases());
        }

        boolean mouvement = fusionTableauDeuxDimension();

        if (sensDeLecture) {
            OperationMatricielle.renverserTableau2D(this.getCases());
        }
        if (vertical) {
            OperationMatricielle.transposerTableau(this.getCases());
        }

        if (mouvement) {
            Deplacements.miseAJoursPositions(this.getCases());

            this.derniersDeplacements = Deplacements.genererDeplacements(this.getCases());
        } else {
            this.derniersDeplacements = null;
        }

        return mouvement;
    }

    /**
     * Permet d'initialiser le premier déplacement
     */
    public void initialiserMouvement() {
        Deplacements.initialiserPositions(this.getCases());
        Deplacements.miseAJoursPositions(this.getCases());
        this.derniersDeplacements = Deplacements.genererDeplacements(this.getCases());
    }

    /**
     *
     * @return le score de la grille
     */
    public int getScore() {
        return score;
    }

    /*------------------------------------------------------------------------------
 * Creation des cases
 *----------------------------------------------------------------------------*/
    /**
     * Genere deux cellules de maniere aléatoire conformément aux règles
     * d'initialisation du jeu
     */
    public void genererAleatoirementDeuxCellules() {

        // Crée une liste de toutes les coordonnées des cases vides
        ArrayList<int[]> casesVides = casesVides();

        // Choisix deux cases parmis les coordonnees disponibles
        int[] cellule1 = choisirCoordonneesAleatoires(casesVides);
        int[] cellule2 = choisirCoordonneesAleatoires(casesVides);

        // Genere le rang de la premiere case
        int rang1 = OperationAleatoire.genererRangAleatoire();

        // Genere le rang de la deuxieme case et positionne les deux cases
        // Cas d'une premiere case de rang 1 : la deuxieme case à pour rang 0
        if (rang1 == 1) {
            creerUneCellule(cellule1[0], cellule1[1], rang1);
            creerUneCellule(cellule2[0], cellule2[1], 0);

            /* Cas d'une premiere case de rang 0 :
         * la deuxieme case à un rang aléatoire */
        } else {
            creerUneCellule(cellule1[0], cellule1[1], rang1);
            creerUneCellule(cellule2[0], cellule2[1],
                    OperationAleatoire.genererRangAleatoire());
        }
    }

    /**
     * Genere une cellule de maniere aléatoire
     */
    public void genererAleatoirementUneCellule() {
        int rang = OperationAleatoire.genererRangAleatoire();

        if (!OperationEvaluation.estComplete(this)) {
            int[] cellule = choisirCoordonneesAleatoires(casesVides());
            creerUneCellule(cellule[0], cellule[1], rang);
        }
    }

    /**
     * @param casesVides liste
     * @return renvoie un élément aléatoire de la liste passée en paramètre, et
     * enleve cet element de la liste
     */
    private int[] choisirCoordonneesAleatoires(ArrayList<int[]> casesVides) {
        Collections.shuffle(casesVides);
        return casesVides.remove(0);
    }

    /**
     * Crée une liste de toutes les coordonnées des cases vides
     *
     * @return liste des coordonnées des cases vides
     */
    private ArrayList<int[]> casesVides() {
        ArrayList<int[]> casesVides = new ArrayList<>();

        // Crée une liste de toutes les coordonnées des cases vides
        for (int i = 0; i < TAILLE; i++) {
            for (int j = 0; j < TAILLE; j++) {
                if (Case.estVide(this.getCases()[i][j])) {
                    // Crée les coordonnées
                    int[] coordonnees = new int[]{i, j};

                    // Ajoute les coordonnées à la liste des cases vides
                    casesVides.add(coordonnees);
                }
            }
        }

        return casesVides;
    }

    /**
     * Initialise la grille avec des cases vides
     */
    private void initialiserGrilleVide() {
        for (int i = 0; i < TAILLE; i++) {
            for (int j = 0; j < TAILLE; j++) {
                creerUneCellule(i, j, -1);
            }
        }
    }

    /**
     * Modifie la valeur contenu de le tableau des cases
     *
     * @param x
     * @param y
     * @param rang
     */
    private void creerUneCellule(int x, int y, int rang) {
        if (x < TAILLE && x > -1 && y < TAILLE && y > -1) {
            if (rang < 0) {
                this.cases[x][y] = Case.VIDE;
            } else {
                this.cases[x][y] = new Case(rang);
            }
        }
    }

    /*------------------------------------------------------------------------------
 * Fusion des cases (mouvement dans une direction) 
 *----------------------------------------------------------------------------*/
    /**
     *
     * @return si une fusion a été effectué dans un tableau à 2 dimensions.
     */
    private boolean fusionTableauDeuxDimension() {
        boolean mouvement = false;
        for (int i = 0; i < TAILLE; i++) {
            mouvement |= fusionTableauUneDimension(this.getCases()[i]);
        }
        if (mouvement) {
            genererAleatoirementUneCellule();
        }
        return mouvement;
    }

    /**
     * Algorithme générique de traitement des fusions sur un tableau à une
     * dimension
     *
     * @param tableau
     * @return vrai si une fusion à été effectué
     */
    private boolean fusionTableauUneDimension(Case[] tableau) {

        /* On réalise une copie du tableau en entrée pour vérifier si des modifications 
           ont été engendrées par l'algorithme
         */
        Case[] stockageCopieTableau = tableau.clone();

        ArrayList<Case> entree = new ArrayList<>();

        // On ajoute le contenu du tableau et enleve les cases vides.
        entree.addAll(Arrays.asList(tableau));
        enleverCasesVides(entree);

        // Fusionne les cases adjacentes si possible
        ArrayList<Case> retour = new ArrayList<>();
        for (int i = 0; i < entree.size(); i++) {
            if (i < entree.size() - 1
                    && Case.sontFusionnables(entree.get(i), entree.get(i + 1))) {
                Case caseFusionnee = entree.get(i).fusionner(entree.get(i + 1));
                score += SUITE[caseFusionnee.getRang()];
                retour.add(caseFusionnee);
                entree.remove(i + 1);

            } else {
                retour.add(entree.get(i));
            }
        }

        // Remplis le tableau de cases vides
        for (int i = 0; i < TAILLE; i++) {
            // Remplis le tableau de cases vides si aucune case n'est disponible
            if (retour.isEmpty()) {
                tableau[i] = Case.VIDE;

                // Ajoute les cases au tableau
            } else {
                tableau[i] = retour.remove(0);
            }
        }
        //On retourne la comparaison entre 
        return !sontEgales(stockageCopieTableau, tableau);
    }

    /**
     * @param mouvement
     * @param tableau
     * @return vrai si les deux tableaux sont egaux
     */
    private static boolean sontEgales(Case[] mouvement, Case[] tableau) {
        for (int i = 0; i < TAILLE; i++) {
            if (!Case.sontEgales(mouvement[i], tableau[i])) {
                return false;
            }
        }
        return true;
    }

    /**
     * Retire les cases vides dans une arraylist
     *
     * @param entree
     */
    private void enleverCasesVides(ArrayList<Case> entree) {
        for (Iterator<Case> iterator = entree.iterator(); iterator.hasNext();) {
            if (Case.estVide(iterator.next())) {
                iterator.remove();
            }
        }
    }

    /*------------------------------------------------------------------------------
 * Evaluation de l'état de la grille
 *----------------------------------------------------------------------------*/
    /**
     * Appel la fonction de création d'un état en fonction de l'évaluation
     * courante de la partie
     *
     * @return l'etat de la partie
     */
    public Etat evaluerEtat() {
        if (OperationEvaluation.estGagnee(this)) {
            return Construction.creerUnEtat(ETAT_GAGNE);
        } else {
            if (OperationEvaluation.estPerdue(this)) {
                return Construction.creerUnEtat(ETAT_PERDU);
            }
            return Construction.creerUnEtat(ETAT_NON_TERMINE);
        }
    }
}
