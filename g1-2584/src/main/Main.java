/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import controleur.ControleurMenuPrincipal;
import controleur.ControleurPartie;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Classe de démarrage étend la classe de Application de javafx
 *
 * @author mg
 */
public class Main extends Application {

    /**
     * Fonction d'execution du jeu 2584
     *
     * @param args arguments de ligne de commande
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Démarrage de la GUI
     *
     * @param theatre théâtre dans lequel créer les scènes du jeu
     * @throws Exception
     */
    @Override
    public void start(Stage theatre) throws Exception {
        ControleurMenuPrincipal menu = new ControleurMenuPrincipal(theatre);

        // démarage de la GUI
        menu.demarerGUI(theatre);

        // initialisation de l'apparence globale du theatre
        theatre.setTitle("2584 groupe 1");

        // affichage du théâtre
        theatre.show();
    }

}
