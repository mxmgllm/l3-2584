# G1-2584
> Réalisation 2584 à 2 joueurs en Java(variante du jeu 2048 avec la suite de Fibonacci )

## Auteurs
 - MARQUER Esteban
 - GUILLAUME Maxime
 
## Spécifications fonctionnelles
1. [x] Jeu à 2 joueurs sur le même ordinateur sans interface graphique dans la console Netbeans(vous pouvez vous inspirer du TP réalisé en L2 !)

1. [x] Jeu à 2 joueurs sur le même ordinateur avec interface graphique
1. [x] Affichage fluide dans l’interface graphique avec des Threads
1. [x] Possibilité pour chaque joueur d’annuler son dernier déplacement en cliquant sur un bouton undo (il ne peut annuler que le dernier coup, suite à quoi le bouton se grise jusqu’au prochain déplacement et il ne peut utiliser cette fonctionnalité que 5 fois par partie)
1. [x] Possibilité que le joueur 2 soit joué par un ordinateur avec des coups aléatoires (l’ordinateur effectue un déplacement à chaque fois que le joueur 1 humain en effectue un également)
1. [x] Possibilité que le joueur 1 soit une I.A. pouvant affronter un joueur 2 (humain ou ordinateur avec mouvements aléatoires). Le but de l’I.A. est de perdre le plus rapidement possible. On comparera en particulier par rapport aux mouvements aléatoires le jour de la soutenance;
1. [x] Possibilité de recharger la partie qui était en cours lors du précédent lancement du jeu (grâce à la sérialisation)
1. [x] Possibilité de voir le résultat des 10 dernières parties (score de chaque joueur, tuile maximum atteinte et nombre de déplacements effectués). Les données sont stockées en base de données et affichées dans le programme Java

## Lancer le programme
- Se placer à la racine du projet dans le terminal;
- lancer la commande : ```java -jar jar/g1-2584.jar  ```.
